﻿<%@ WebHandler Language="C#" Class="GetProductInfo" %>

using System;
using System.Web;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;


public class GetProductInfo : IHttpHandler
{
    common c = new common();
    ImagePicture_DAL imagePicture = new ImagePicture_DAL();
    Product_Model product = new Product_Model();
    ProductScore_Model productScore = new ProductScore_Model();
    public DataView dv_productInfo = new DataView();
    public DataTable dt_productInfo = new DataTable();

    public void ProcessRequest(HttpContext context)
    {
        string[] finalImgIdString = Regex.Split(context.Request.QueryString["finalImgIdString"], "_id_", RegexOptions.IgnoreCase);
        string[] finalTermLeaderString = Regex.Split(context.Request.QueryString["finalTermLeaderString"], "_term_", RegexOptions.IgnoreCase);


        dt_productInfo.Columns.Add("p_name");
        dt_productInfo.Columns.Add("p_imgUrl");
        dt_productInfo.Columns.Add(finalTermLeaderString[0]);
        dt_productInfo.Columns.Add(finalTermLeaderString[1]);
        getProductInfo(context, finalImgIdString, finalTermLeaderString);
    }
    /// <summary>
    /// 產生json_20161025_howard
    /// </summary>
    /// <param name="finalImgIdString"></param>
    /// <param name="finalTermLeaderString"></param>
    public void getProductInfo(HttpContext context, string[] finalImgIdString, string[] finalTermLeaderString)
    {
        //取得產品基本資訊以及X Y分數
        for (int i = 1; i <= finalImgIdString.Length - 1; i++)
        {
            productScore.p_id = Convert.ToInt32(finalImgIdString[i]);
            for (int j = 0; j < finalTermLeaderString.Length - 1; j++)
            {
                productScore.ps_termLeader += finalTermLeaderString[j] + "_xy_";
            }
            dv_productInfo = imagePicture.selectProductInfo(productScore);

            DataTable dt_tempProductInfo = dv_productInfo.ToTable();
            DataRow workRow = dt_productInfo.NewRow();
            int rowCount = 0;
            foreach (DataRow row in dt_tempProductInfo.Rows)
            {
                if (rowCount == 0)
                {
                    workRow["p_name"] = row["p_name"];

                    workRow["p_imgUrl"] = c.DoRetrunSpecialChar(Convert.ToString(row["p_imgUrl"]));
                    //workRow[finalTermLeaderString[0]] = row["ps_score"];
                    workRow[row["ps_termLeader"].ToString()] = row["ps_score"];

                    rowCount += 1;
                }
                else
                {
                    //workRow[finalTermLeaderString[1]] = row["ps_score"];
		            workRow[row["ps_termLeader"].ToString()] = row["ps_score"];
                }
            }
            dt_productInfo.Rows.Add(workRow);
        }

        //產生 json
        string result = JsonConvert.SerializeObject(@dt_productInfo, new DataTableConverter());
        //Response.Write("<Script language='JavaScript'>alert('" + result + "');</Script>");//測試用

        context.Response.Clear();
        context.Response.ContentType = "application/json; charset=utf-8";
        context.Response.Write(result);
        context.Response.End();


    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}