﻿using System;
using System.Net;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web;
using System.Data;

public partial class WebPage_SemanticSearch : System.Web.UI.Page
{
    public DataView cartData;
    public static Rootobject ro = new Rootobject();
    Dictionary<string, string> roresults = new Dictionary<string, string>();
    Dictionary<string, string> roresults_score = new Dictionary<string, string>();

    SemanticSearch_DAL semanticSearch = new SemanticSearch_DAL();
    Cart_Model cart = new Cart_Model();
    Product_Model product = new Product_Model();
    ProductScore_Model productScore = new ProductScore_Model();
    CartContent_Model cartContent = new CartContent_Model();
    publicFunction pubFun = new publicFunction();
    /// <summary>
    /// 登入者帳號
    /// </summary>
    public string loginAccount
    {
        set { ViewState["loginAccount"] = value; }
        get
        {
            if (ViewState["loginAccount"] == null)
                return string.Empty;
            return ViewState["loginAccount"].ToString();
        }
    }

    /// <summary>
    /// 儲存搜尋結果
    /// </summary>
    public Dictionary<string, string> vs_searchResults
    {
        set { ViewState["searchResults"] = value; }
        get
        {
            if (ViewState["searchResults"] == null)
                return null;
            return (Dictionary<String, String>)ViewState["searchResults"];
        }
    }

    /// <summary>
    /// 儲存搜尋結果_各群分數
    /// </summary>
    public Dictionary<string, string> vs_searchResults_score
    {
        set { ViewState["searchResults_score"] = value; }
        get
        {
            if (ViewState["searchResults_score"] == null)
                return null;
            return (Dictionary<string, string>)ViewState["searchResults_score"];
        }
    }

    /// <summary>
    /// Page Load
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {      
        lbl_loginName.Text = Convert.ToString(Session["user_id"]);
        if (IsPostBack)
        { 
            //控制搜尋視窗要不要彈出來
            ClientScript.RegisterClientScriptBlock(GetType(), "IsPostBack", "var isPostBack = true;", true);
        }
        else
        {

            //控制搜尋視窗要不要彈出來
            ClientScript.RegisterClientScriptBlock(GetType(), "IsPostBack", "var isPostBack = false;", true);

            if (Convert.ToString(Session["user_id"]) == "")
            {
                Response.Write("<Script language='JavaScript'>alert('請登入！');</Script>");
                Response.Write("<script>window.parent.location='" + "../WebPage/UserLogin.aspx" + "'</script>");
            }
            else
            {
                vs_searchResults = null;

                //測試帳號:我自己
                //loginAccount = "A50356";//這裡之後要自己讀取帳號
                loginAccount = Convert.ToString(Session["user_id"]);

                cart.user_id = loginAccount;

                //lbtn_searchBar.Attributes.Add("onclick", string.Format("OpenWin('{0}','{1}');return false;", "", "../SubWin/SearchBar.aspx"));

                //讀取購物車資料，呈現購物車圖案
                cartData = semanticSearch.howManyCarts(cart);
                rpt_cartCount.DataSource = cartData;
                rpt_cartCount.DataBind();

                ddl_myCollection.DataValueField = "c_id";
                ddl_myCollection.DataTextField = "c_name";
                ddl_myCollection.DataSource = cartData;
                ddl_myCollection.DataBind();

                if (cartData.Count == 0)
                    rbtn_AddOld.Enabled = false;

                ////呼叫台大API
                //if (Request.QueryString["Query"] != null)
                //{
                //    callAPI("http://nlg18.csie.ntu.edu.tw:9010/search/search/?q=" + Request.QueryString["Query"] + "&lang=" + Request.QueryString["lang"] + "&area=" + Request.QueryString["Area"] + "&location=" + Request.QueryString["Location"] + "&price_lower=" + Request.QueryString["price_lower"] + "&price_upper=" + Request.QueryString["price_upper"] + "&usd_ntd=31.7&rmb_ntd=4.7&offset=0&limit=100&view=&fast=false");
                //    txt_collection_name.Text = Request.QueryString["Query"];
                //}


                //從cookie撈出之前的搜尋值 
                    if (Request.Cookies["Query"] != null)
                    { tbx_query.Text = Request.Cookies["Query"].Value; }

                    if (Request.Cookies["AreaMenu"] != null)
                    {
                        rbtn_area_all.Checked = false;
                        rbtn_area_living.Checked = false;
                        rbtn_area_mombaby.Checked = false;
                        rbtn_area_office.Checked = false;
                        rbtn_area_leisure.Checked = false;

                        switch (Request.Cookies["AreaMenu"].Value)
                        {
                            case "":
                                rbtn_area_all.Checked = true;
                                break;
                            case "living":
                                rbtn_area_living.Checked = true;
                                break;
                            case "mombaby":
                                rbtn_area_mombaby.Checked = true;
                                break;
                            case "office":
                                rbtn_area_office.Checked = true;
                                break;
                            case "leisure":
                                rbtn_area_leisure.Checked = true;
                                break;
                        }
                    }

                    if (Request.Cookies["LocationMenu"] != null)
                    {
                        rbtn_location_all.Checked = false;
                        rbtn_location_livingroom.Checked = false;
                        rbtn_location_garden.Checked = false; 

                        switch (Request.Cookies["LocationMenu"].Value)
                        {
                            case "":
                                rbtn_location_all.Checked = true;
                                break;
                            case "客廳":
                                rbtn_location_livingroom.Checked = true;
                                break;
                            case "花園":
                                rbtn_location_garden.Checked = true;
                                break; 
                        }
                    }

                    //Price Slider直接前台處理 
                    if (Request.Cookies["lang"] != null)
                    {
                        rbtn_lang_all.Checked = false;
                        rbtn_lang_en.Checked = false;
                        rbtn_lang_zh.Checked = false;

                        switch (Request.Cookies["lang"].Value)
                        {
                            case "":
                                rbtn_lang_all.Checked = true;
                                break;
                            case "en":
                                rbtn_lang_en.Checked = true;
                                break;
                            case "zh":
                                rbtn_lang_zh.Checked = true;
                                break;
                        }
                    }
                     
                  
            }


        }

    }

    #region jSon Crawler
    /// <summary>
    /// call api to get json_20160922_howard
    /// </summary>
    /// <param name="url"></param>
    /// <returns></returns>
    public void callAPI(string Query, string lang , string AreaMenu , string LocationMenu, string price_lower , string  price_upper)
    {

        string url = "http://nlg18.csie.ntu.edu.tw:9010/search/search/?q=" + Query + "&lang=" + lang + "&area=" + AreaMenu + "&location=" + LocationMenu + "&price_lower=" + price_lower + "&price_upper=" + price_upper + "&usd_ntd=31.7&rmb_ntd=4.7&offset=0&limit=100&view=&fast=false";


        //得到指定 url 的原始碼
        string strResult = "";
        //System.Net.ServicePointManager.DefaultConnectionLimit = 200;
        try
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            //聲明一個HttpWebRequest請求
            request.Timeout = 30000000;
            //request.Proxy = null;
            //request.KeepAlive = false;
            //request.ProtocolVersion = HttpVersion.Version10;
            //request.ServicePoint.ConnectionLimit = 12;
            //設置連接逾時時間
            //request.Headers.Set("Pragma", "no-cache");
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream streamReceive = response.GetResponseStream();
            Encoding encoding = Encoding.GetEncoding("GB2312");
            StreamReader streamReader = new StreamReader(streamReceive, encoding);
            strResult = streamReader.ReadToEnd();

            Response.Write("<Script language='JavaScript'>alert('" + strResult + "！');</Script>");
        }
        catch (Exception ex)
        {
            throw new Exception(string.Format("Crawler，錯誤訊息{0}", ex));
        }
        System.GC.Collect();

        //網頁原始碼前置處理
        string strWebContent = strResult;
        strWebContent = unicodeToGB(strWebContent);
        strWebContent = Regex.Replace(strWebContent, "[\r\n\t]", string.Empty, RegexOptions.Compiled);
        strWebContent = Regex.Replace(strWebContent, "<b>", string.Empty, RegexOptions.Compiled);
        strWebContent = Regex.Replace(strWebContent, "</b>", string.Empty, RegexOptions.Compiled);
        strWebContent = Regex.Replace(strWebContent, "<b/>", string.Empty, RegexOptions.Compiled);
        strWebContent = Regex.Replace(strWebContent, "<br>", string.Empty, RegexOptions.Compiled);
        strWebContent = Regex.Replace(strWebContent, "</br>", string.Empty, RegexOptions.Compiled);
        strWebContent = Regex.Replace(strWebContent, "<br/>", string.Empty, RegexOptions.Compiled);
        strWebContent = strWebContent.Replace("results(", "");
        strWebContent = strWebContent.Replace(");", "");


        //綁定搜尋結果以瀑布流顯示
        //搜尋條件沒輸入就不秀_20161024_howard
        //lbl_condition.Text = (Request.QueryString["Query"] == "" ? "" : ("關鍵字 - " + Request.QueryString["Query"] + "；"));
        //lbl_condition.Text += (Request.QueryString["Area"] == "" ? "" : ("產品 - " + Request.QueryString["Area"] + "；"));
        //lbl_condition.Text += (Request.QueryString["Location"] == "" ? "" : ("地點 - " + Request.QueryString["Location"] + "；"));
        //lbl_condition.Text += ((Request.QueryString["price_lower"].IndexOf("-1") > -1 && Request.QueryString["price_upper"].IndexOf("100000") > -1) ? "" : ("價格 - " + Request.QueryString["price_lower"] + "~" + Request.QueryString["price_upper"] + "；"));
        //lbl_condition.Text += (Request.QueryString["lang"] == "" ? "" : ("語言 - " + Request.QueryString["lang"] + ""));

        lbl_condition.Text = (Query == "" ? "" : ("關鍵字 - " + Query + "；"));
        lbl_condition.Text += (AreaMenu == "" ? "" : ("產品 - " + AreaMenu + "；"));
        lbl_condition.Text += (LocationMenu == "" ? "" : ("地點 - " + LocationMenu + "；"));
        lbl_condition.Text += ((price_lower.IndexOf("-1") > -1 && price_upper.IndexOf("100000") > -1) ? "" : ("價格 - " + price_lower + "~" + price_upper + "；"));
        lbl_condition.Text += (lang == "" ? "" : ("語言 - " + lang + ""));



        ro = JsonConvert.DeserializeObject<Rootobject>(@strWebContent);
        lbl_imageTerms.Text = ro.imageterms.Replace(" ","；");

        foreach (Result result in ro.results)//將搜尋結果存進 dictionary then assign to viewstate_20161017_howard
        {
            try
            {
                roresults.Add(result.imUrl, result.title + "_item_" + result.price + "_item_" + result.text + "_item_" + result.link);

                //存分數_20161116_howard
                string scoreString = "";
                foreach (object value in result.image_term_distribution)
                {

                    scoreString += Regex.Replace(Convert.ToString(value), "[\r\n\t]", string.Empty, RegexOptions.Compiled) + "_item_";
                }
                scoreString = scoreString.Replace("[", "");
                scoreString = scoreString.Replace("]", "");
                scoreString = scoreString.Replace(" ", "");
                scoreString = scoreString.Replace("\"", "");
                roresults_score.Add(result.imUrl, scoreString);

                //資料處理
                result.lang_fix = (result.lang == "zh") ? "中" : "英";
                if (result.lang_fix == "中")
                    result.title_fix = (result.title.Length > 23) ? result.title.Substring(0, 23) + "..." : result.title;
                else if(result.lang_fix == "英")
                    result.title_fix = (result.title.Length > 40) ? result.title.Substring(0, 40) + "..." : result.title;

                if (result.lang_fix == "中")
                    result.text = (result.text.Length > 150) ? result.text.Substring(0, 150) + "..." : result.text;
                else if (result.lang_fix == "英")
                    result.text = (result.text.Length > 250) ? result.text.Substring(0, 250) + "..." : result.text;

            }
            catch (Exception e)
            {
                continue;
            }
        }
        vs_searchResults = roresults;
        vs_searchResults_score = roresults_score;
        rpt_productContents.DataSource = ro.results;
        rpt_productContents.DataBind();
    }

    /// <summary>
    /// unicodeToGB_20160922_howard
    /// </summary>
    /// <param name="text"></param>
    /// <returns></returns>
    public static string unicodeToGB(string text)
    {
        System.Text.RegularExpressions.MatchCollection mc = System.Text.RegularExpressions.Regex.Matches(text, "\\\\u([\\w]{4})");
        if (mc != null && mc.Count > 0)
        {
            foreach (System.Text.RegularExpressions.Match m2 in mc)
            {
                string v = m2.Value;
                string word = v.Substring(2);
                byte[] codes = new byte[2];
                int code = Convert.ToInt32(word.Substring(0, 2), 16);
                int code2 = Convert.ToInt32(word.Substring(2), 16);
                codes[0] = (byte)code2;
                codes[1] = (byte)code;
                text = text.Replace(v, Encoding.Unicode.GetString(codes));
            }
        }
        else
        {

        }
        return text;
    }

    #endregion
    
    /// <summary>
    /// 點選目前購物車按鈕確認存入資料庫_20161014_howard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_goToCart_Click(object sender, EventArgs e)
    {
        //先確認是否有勾選任何商品
        if (HttpContext.Current.Request.Cookies["imgUrlString"] == null)
            return;
        string[] imgUrlString = Regex.Split(HttpContext.Current.Request.Cookies["imgUrlString"].Value, "_url_", RegexOptions.IgnoreCase);
        if (imgUrlString.Length == 1)
            return;
        
        //*存資料進資料庫*
        //1.存進購物車資料表 Cart
        //cart.c_name = Request.QueryString["Query"];
        //cart.c_name = lbl_condition.Text;//購物車名稱是所有有輸入的搜尋條件合併_20161024_howard
        cart.c_name = txt_collection_name.Text;
        cart.c_conditions = lbl_condition.Text;
        cart.c_imgTerms = lbl_imageTerms.Text;
        cart.user_id = loginAccount;
        if (rbtn_AddNew.Checked) {//產生新的收藏夾 


            if (semanticSearch.isCartNameExist(cart.c_name, cart.user_id))
            {
                Response.Write("<script>alert('加入失敗，此收藏夾已存在')</script>");
                return;
            }
            else if (cart.c_name == "")
            {
                Response.Write("<script>alert('加入失敗，收藏夾名稱不能為空')</script>");
                return;
            }
            else 
                cart.c_id = semanticSearch.InsertIntoCart(cart);
            
        } else if (rbtn_AddOld.Checked)//存進舊的收藏夾
        {
            cart.c_id = Int32.Parse(ddl_myCollection.SelectedValue.ToString()); 
        }
        //2.存進產品清單資料表 Product 和產品語意分數資料表 ProductScore，先檢查產品是否已存在，已存在則不用 insert
        //3.存進購物車內容資料表 CartContent
        
        
        for (int i = 1; i <= imgUrlString.Length - 1; i++)
        {
            product.p_imgUrl = imgUrlString[i];
            string[] tempResult = Regex.Split(vs_searchResults[product.p_imgUrl], "_item_", RegexOptions.IgnoreCase);
            product.p_name = tempResult[0];
            product.p_price = tempResult[1];
            product.p_review = tempResult[2];
            product.p_oriUrl = tempResult[3];//新增產品原始網址_20161026_howard


            //存進產品清單資料表 Product
            product.p_id = semanticSearch.InsertIntoProduct(product);

            //存進產品語意分數資料表 ProductScore
            if (product.p_id != 0)//如果還沒存過才存
            {
                //存語意分數_20161116_howard
                string[] tempResult2 = Regex.Split(vs_searchResults_score[product.p_imgUrl], "_item_", RegexOptions.IgnoreCase);

                for (int j = 0; j <= 25; j++)
                {
                    string[] tempScore = tempResult2[j].Split(',');
                    string termLeader = tempScore[0];
                    double termScore = Convert.ToDouble(tempScore[1]);
                    productScore.ps_id = semanticSearch.InsertIntoProductScore(product, termLeader, termScore);
                }
            }
            //存進購物車內容資料表 CartContent
            cartContent.c_id = cart.c_id;
            cartContent.p_id = product.p_id;
            cartContent.cc_id = semanticSearch.InsertIntoCartContent(cartContent);
        }

        //用登入帳號進入購物車頁面
        Response.Redirect("ProductCart.aspx?loginName=" + cart.user_id);

    }
    /// <summary>
    /// 去搜尋_20161103_howard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_goToSearch_Click(object sender, EventArgs e)
    {
        Response.Write("<script>window.parent.location='" + "../WebPage/SemanticSearch.aspx?loginName=" + lbl_loginName.Text + "'</script>");
    }
    /// <summary>
    /// 登出_20161103_howard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_logout_Click(object sender, EventArgs e)
    {

        Session["user_id"] = "";
        Session["user_st"] = "";
        Response.Write("<script>window.parent.location='" + "../WebPage/index.aspx" + "'</script>");
        lbtn_logout.Visible = false;

    }

    /// <summary>
    /// 搜尋呼叫API
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_search_OnClick(object sender, EventArgs e)
    {
        if (tbx_query.Text == "")
        {
            Response.Write("<script>alert('搜尋條件必填！')</script>");
        }
        else
        {
            //參數
            string Query = tbx_query.Text;
            string AreaMenu = "";
            string LocationMenu = "";
            string price_lower = "0";
            string price_upper = "100000";
            string lang = ""; 

            if (rbtn_area_living.Checked)
            {
                AreaMenu = "living";
            }
            else if (rbtn_area_mombaby.Checked)
            {
                AreaMenu = "mombaby";
            }
            else if (rbtn_area_office.Checked)
            {
                AreaMenu = "office";
            }
            else if (rbtn_area_leisure.Checked)
            {
                AreaMenu = "leisure";
            }

            if (rbtn_location_livingroom.Checked)
            {
                LocationMenu = "客廳";
            }
            else if (rbtn_location_garden.Checked)
            {
                LocationMenu = "花園";
            }
            string[] select_price_box = Request.Form["hf_select_price"].Split(',');
            if (select_price_box.Length == 2)
            { 
                price_lower = (select_price_box[0] == "NaN" ? "0": select_price_box[0]);
                price_upper = (select_price_box[1] == "NaN" ? "100000" : select_price_box[1]); 
            }
            if (rbtn_lang_en.Checked)
            {
                lang = "en";
            }
            else if (rbtn_lang_zh.Checked)
            {
                lang = "zh";
            }

            //hf_jsonQuery.Value = "../WebPage/SemanticSearch.aspx?&Query=" + Query + "&Area=" + AreaMenu + "&Location=" + LocationMenu + "&price_lower=" + price_lower + "&price_upper=" + price_upper + "&lang=" + lang;
            //Response.Write("<script>window.parent.location='" + "../WebPage/SemanticSearch.aspx?&Query=" + Query + "&Area=" + AreaMenu + "&Location=" + LocationMenu + "&price_lower=" + price_lower + "&price_upper=" + price_upper + "&page=" + page + "&lang=" + lang + "'</script>");


            //將搜尋值存到cookie 
            Response.Cookies["Query"].Value = tbx_query.Text;
            Response.Cookies["AreaMenu"].Value = AreaMenu;
            Response.Cookies["LocationMenu"].Value = LocationMenu;
            Response.Cookies["PriceLower"].Value = price_lower;
            Response.Cookies["PriceUpper"].Value = price_upper;
            Response.Cookies["lang"].Value = lang; 


            callAPI( Query ,lang, AreaMenu, LocationMenu, price_lower , price_upper);
            txt_collection_name.Text = Query;


        }
    }
     

}