﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProductCart.aspx.cs" EnableEventValidation="false" Inherits="WebPage_ProductCart" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Bootstrap core CSS -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/bootstrap.css" rel="stylesheet">
    <!-- jQuery load -->
    <script src="../js/jquery.min.js"></script>

    <!-- Custom styles for this template -->
    <link href="../style.css" rel="stylesheet">
    <link href="../css/plugin.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/colorbox.css" media="all" />
    <!-- Revolutions slider -->

    <link href="../rs-plugin/css/settings.css" rel="stylesheet">
    <link href="../rs-plugin/css/captions.css" rel="stylesheet">
    <!-- multi selector -->
    <link href="../css/multiple-select.css" rel="stylesheet">
    <link href="../css/jquery.powertip.css" rel="stylesheet" type="text/css" />


    <!-- 160225 add start -->
    <link href="../css/dcaccordion.css" rel="stylesheet">
    <link href="../css/skins/grey.css" rel="stylesheet" type="text/css" />
    <script type='text/javascript' src='../js/jquery.checkall.js'></script>
    <script type='text/javascript' src='../js/jquery.cookie.js'></script>
    <script type="text/javascript" src="../js/jquery.hoverIntent.minified.js"></script>
    <script type="text/javascript" src="../js/jquery.dcjqaccordion.2.7.min.js"></script>
    <script type="text/javascript" src="../js/masonry.pkgd.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if (jQuery.fn.cssOriginal != undefined)
                jQuery.fn.css = jQuery.fn.cssOriginal;

            if (jQuery('#rev_slider').revolution == undefined)
                revslider_showDoubleJqueryError('#rev_slider');

            //checkall
            $('.check-all').checkAll();

            $('.accordion-1').dcAccordion({
                eventType: 'click',
                autoClose: false,
                saveState: false,
                disableLink: true,
                showCount: true,
                speed: 'slow'
            });
            /* 頁籤式選單:若單頁出現兩個以上,需再加入ID */
            $(".tabmenuT2Btn").click(function () {
                $(".tabmenuT2Btn").removeClass("tabmenuT2BtnCurrent");
                $(this).addClass("tabmenuT2BtnCurrent");
            })

            //瀑布流
            $('.grid').masonry({
                // options...
                itemSelector: '.grid-item',
                columnWidth: 250
            });

            //瀑布法END


            //清除舊cookie避免有殘留資料
            document.cookie = "finalImgUrlString=";
            document.cookie = "finalImgIdString=";
        });

    </script>
    <!-- 160225 add end -->


    <%-- 導向搜尋頁面_20161018_howard --%>
    <script type="text/javascript">
        function goToSearch() {
            window.location = "SemanticSearch.aspx?loginName=" + document.getElementById("lbl_loginName").textContent;
        }


    </script>


    <%-- 刪除購物車_20161018_howard --%>
    <script type="text/javascript">
        function deleteCart(c_id) {
            if (confirm("是否確定刪除此收藏夾?")) {
                window.location = "ProductCart.aspx?loginName=" + document.getElementById("lbl_loginName").textContent + "&deleted_c_id=" + c_id;
            }
        }
    </script>

    <%-- 修改購物車名稱_20161018_howard --%>
    <script type="text/javascript">
        function updateCartName(c_id, new_c_name) {
            if (confirm("是否確定修改此收藏夾名稱?")) {
                window.location = "ProductCart.aspx?loginName=" + document.getElementById("lbl_loginName").textContent + "&updated_c_id=" + c_id + "&new_c_name=" + new_c_name;
            }
            else {
                window.location = "ProductCart.aspx?loginName=" + document.getElementById("lbl_loginName").textContent;
            }

        }
    </script>

    <%-- 點選購物車_20161018_howard --%>
    <script type="text/javascript">
        var c_idString = '';

        function checkCart(c_id) {
            var tempCount = parseInt(document.getElementById("lbl_productCount").textContent);
            if (c_idString.search(c_id) == -1) {
                c_idString = c_idString + '_id_' + c_id;
                document.cookie = "c_idString=" + c_idString;
                document.getElementById('<%=lbl_productCount.ClientID %>').textContent=tempCount+1;
            }
            else {
                c_idString = c_idString.replace('_id_' + c_id, "");
                document.cookie = "c_idString=" + c_idString;
                document.getElementById('<%=lbl_productCount.ClientID %>').textContent=tempCount-1;
            }


            if (c_idString == '')
                document.getElementById("lbtn_confirmCheck").setAttribute('disabled', 'disabled');
            else
                document.getElementById("lbtn_confirmCheck").removeAttribute('disabled');
            
        }
        function clearCart() {
            $.cookie('c_idString', "");
                document.getElementById('<%=lbl_productCount.ClientID %>').textContent= 0 ;
        }

    </script>




    <%-- 準備_20161018_howard --%>
    <script type="text/javascript">
        var finalImgUrlString = '';
        var finalImgIdString = '';
        function checkImg(imgUrl, imgId) {
            //判斷圖片網址字串是否已經存在
            if (finalImgUrlString.search('_url_' + imgUrl) == -1) {
                finalImgUrlString = finalImgUrlString + '_url_' + imgUrl;
                document.cookie = "finalImgUrlString=" + finalImgUrlString;
            }
            else {

                finalImgUrlString = finalImgUrlString.replace('_url_' + imgUrl, "");
                document.cookie = "finalImgUrlString=" + finalImgUrlString;
            }

            //判斷圖片ID字串是否已經存在
            if (finalImgIdString.search('_id_' + imgId) == -1) {
                finalImgIdString = finalImgIdString + '_id_' + imgId;
                document.cookie = "finalImgIdString=" + finalImgIdString;
                 
            }
            else {

                finalImgIdString = finalImgIdString.replace('_id_' + imgId, "");
                document.cookie = "finalImgIdString=" + finalImgIdString; 
            }
             
        }
    </script>
    <style> 
        .DownBlueBar { 
            background: #b0b0b0;
            color: #fff;
            width: 100%;
            padding: 10px;
            display: inline-block;
            text-align: center;
            font-size: 16px;
            text-decoration: none;
        }
        .DownGrayBar { 
            background: #C0C0C0;
            color: #fff;
            width: 100%;
            padding: 10px;
            display: inline-block;
            text-align: center;
            font-size: 16px;
            text-decoration: none;
        }
        .DownBarTitle { 
            float:right;
            color: #fff; 
            font-size: 18px; 
            text-align: center; 
            padding: 10px;
            position:absolute;
        }
        .btn-blue {
	        background-color: #56bbf0;
            color: #FFF;
	        font-weight: bold;
	        border-radius: 4px; 
        }
    </style>
    <%-- 紀錄被選擇的詞彙班長_20161025_howard --%>
    <script type="text/javascript">
        var count = 0;
        var finalTermLeaderString = '';
        function GetSelectedItem() {
            var CHK = document.getElementById("<%=cbxl_termLeader.ClientID%>");
            var checkbox = CHK.getElementsByTagName("input");
            var label = CHK.getElementsByTagName("label");
            for (var i = 0; i < checkbox.length; i++) {
                if (checkbox[i].checked) {
                    finalTermLeaderString += encodeURIComponent(label[i].innerHTML) + '_term_';
                    document.cookie = "finalTermLeaderString=" + finalTermLeaderString;
                    count += 1;
                }
            }
            //導向畫圖頁面 
            if(count < 2)
            {
                alert("請選擇兩個維度 (意象詞)後再進行意象分析"); 
                count = 0;
                finalTermLeaderString = "";
                document.cookie = "finalTermLeaderString="; 
            }
            else if (count > 2) {
                alert("提醒您，最多只能選擇兩個維度 (意象詞)，請重新選擇");
                for (var i = 0; i < checkbox.length; i++) {
                    checkbox[i].checked = false;
                    count = 0;
                    finalTermLeaderString = "";
                    document.cookie = "finalTermLeaderString=" + finalTermLeaderString;
                }
            }
            else {
                //再導向畫圖頁面
                //window.location = "ImagePicture.aspx?loginName=" + document.getElementById("lbl_loginName").textContent;
                window.location = "ProductCart.aspx?loginName=" + document.getElementById("lbl_loginName").textContent;
                window.open("ImagePicture.aspx?loginName=" + document.getElementById("lbl_loginName").textContent, '_blank');
            }
            return false;
        }
    </script>

    <%-- linkbutton 另開視窗_20161024_howard --%>
    <script type="text/javascript">
        function NewWindow() {
            document.forms[0].target = "_blank";
        }
    </script>
    <%-- 意象分析全選 --%>
    <script type="text/javascript">
        function allPick() {
            finalImgUrlString = document.getElementById("hf_AllPick_ImgUrl").value.replace("finalImgUrlString=", "");
            finalImgIdString = document.getElementById("hf_AllPick_ImgID").value.replace("finalImgIdString=", "");
            document.cookie = document.getElementById("hf_AllPick_ImgID").value;
            document.cookie = document.getElementById("hf_AllPick_ImgUrl").value; 

            $(".shoppingcheck3").addClass("shoppingcheckup3");
            
        }
        function allUnPick() {
            finalImgUrlString = '';
            finalImgIdString = '';
            document.cookie = "finalImgIdString="; 
            document.cookie = "finalImgUrlString=";
            $(".shoppingcheck3").removeClass("shoppingcheckup3");
        }

    </script>
    <!--意象分析勾選最多兩項~超過就鎖其他選項-->
    <script type = "text/javascript">

        function termLeaderCantOverTwo()
        {
            var chkBox = document.getElementById('<%= cbxl_termLeader.ClientID %>'); 
            var options = chkBox.getElementsByTagName('input');
            var a = 0;
            for (var i = 0; i < options.length; i++)
            {
                if(options[i].checked)
                {
                    a++;
                }
            }

            if (a >= 2)
                document.getElementById("btn_ToImagePicture").removeAttribute("disabled");
            else
                document.getElementById("btn_ToImagePicture").setAttribute("disabled", "disabled");

            for (var i = 0; i < options.length; i++)
            {
                if (a >= 2) {
                    if (!options[i].checked)
                        options[i].setAttribute("disabled", "disabled");
                }
                else {
                    options[i].removeAttribute("disabled");
                }
            }
            
            

       }

        (function ($) {
            $.fn.goTo = function () {
                $('html, body').animate({
                    scrollTop: $(this).offset().top + 'px'
                }, 'fast');
                return this; // for chaining...
            }
        })(jQuery);

        function scrollTo() {
            $('#analytics').goTo();
            //window.location.hash = '#analytics'; 
        }
        function scrollToTop() {
            $('#rev_slider').goTo();
        }
</script>  

</head>
<body>
    <form id="CartPage" runat="server">
        <script type="text/javascript">
            function Testfunction() {
                //將要使用的jQuery放在這裡
                $(".shoppingcheck3").click(function () {
                    $(this).toggleClass("shoppingcheckup3");
                })
            }
        </script>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

        <script type="text/javascript">
            Sys.WebForms.PageRequestManager.getInstance().add_endRequest(Testfunction);
        </script>

        <div class="rev_slider_wrapper fullscreen-container">
            <div id="rev_slider" class="rev_slider fullscreenbanner" style="overflow: visible; height: 250px !important;">
                <ul>
                    <!-- SLIDE  -->
                    <li data-transition="random" data-slotamount="7" data-masterspeed="300">
                        <!-- MAIN IMAGE -->
                        <img src="../images/sliderbg.jpg" alt="slide1_background" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption sfb" data-x="450" data-y="0" data-speed="300" data-start="500" data-easing="easeOutBounce" data-endspeed="300" style="z-index: 2">
                            <img src="../images/slider1_1.png" alt="">
                        </div>

                    </li>
                    <!-- SLIDE  -->
                    <li data-transition="random" data-slotamount="7" data-masterspeed="300">
                        <!-- MAIN IMAGE -->
                        <img src="../images/sliderbg2.jpg" alt="black_linen_v2" data-bgfit="normal" data-bgposition="center top" data-bgrepeat="repeat">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption sfb" data-x="400" data-y="0" data-speed="300" data-start="500" data-easing="easeOutBounce" data-endspeed="300" style="z-index: 2">
                            <img src="../images/slider1_1.png" alt="">
                        </div>
                    </li>
                    <li data-transition="random" data-slotamount="7" data-masterspeed="300">
                        <!-- MAIN IMAGE -->
                        <img src="../images/sliderbg3.jpg" alt="black_linen_v2" data-bgfit="normal" data-bgposition="center top" data-bgrepeat="repeat">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption sfb" data-x="400" data-y="0" data-speed="300" data-start="500" data-easing="easeOutBounce" data-endspeed="300" style="z-index: 2">
                            <img src="../images/slider1_1.png" alt="">
                        </div>
                    </li>
            </div>
            </li>
	</ul>
        </div>

        <script type="text/javascript">
            var revapi1;
            jQuery(document).ready(function () {

                if (jQuery.fn.cssOriginal != undefined)
                    jQuery.fn.css = jQuery.fn.cssOriginal;

                if (jQuery('#rev_slider').revolution == undefined)
                    revslider_showDoubleJqueryError('#rev_slider');
                else
                    revapi1 = jQuery('#rev_slider').show().revolution(
					{
					    delay: 9000,
					    startwidth: 1170,
					    startheight: 250,
					    hideThumbs: 200,

					    thumbWidth: 100,
					    thumbHeight: 50,
					    thumbAmount: 3,

					    navigationType: "bullet",
					    navigationArrows: "solo",
					    navigationStyle: "round",

					    touchenabled: "on",
					    onHoverStop: "on",

					    navigationHAlign: "center",
					    navigationVAlign: "bottom",
					    navigationHOffset: 0,
					    navigationVOffset: 20,

					    soloArrowLeftHalign: "left",
					    soloArrowLeftValign: "center",
					    soloArrowLeftHOffset: 20,
					    soloArrowLeftVOffset: 0,

					    soloArrowRightHalign: "right",
					    soloArrowRightValign: "center",
					    soloArrowRightHOffset: 20,
					    soloArrowRightVOffset: 0,

					    shadow: 0,
					    fullWidth: "on",
					    fullScreen: "off",

					    stopLoop: "off",
					    stopAfterLoops: -1,
					    stopAtSlide: -1,

					    shuffle: "off",

					    hideSliderAtLimit: 0,
					    hideCaptionAtLimit: 0,
					    hideAllCaptionAtLilmit: 0,
					    startWithSlide: 0,
					    videoJsPath: "js/revslider/js/videojs/",
					    fullScreenOffsetContainer: ""
					});

            }); //ready
        </script>
        <header id="header">
            <div class="top-header">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-12 col-md-12 text-right top-header-right">

                            <div class="header-search pull-right visible-lg visible-md" id="header-search">
                                <form method="get" id="searchform" action="#">
                                    <div class="header-search-input-wrap">
                                        <input class="header-search-input" placeholder="Type to search..." type="text" value="" name="s" id="s" />
                                    </div>
                                    <input class="header-search-submit" type="submit" id="go" value=""><span class="header-icon-search"><i class="fa fa-search"></i></span>
                                </form>
                                <!-- // #searchform -->
                            </div>
                            <!-- // .header-search -->

                            <ul class="social list-unstyled pull-right">
                                <li class="blogger"><a data-toggle="tooltip" data-placement="bottom" title="Blogger Page" href="#"><span><b>B</b></span></a></li>
                                <li class="facebook"><a data-toggle="tooltip" data-placement="bottom" title="Facebook Page" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="twitter"><a data-toggle="tooltip" data-placement="bottom" title="Follow us on Twitter" href="#"><i class="fa fa-twitter"></i></a></li>

                            </ul>
                            <!-- // .social -->

                        </div>
                        <!-- // .top-header-right -->

                    </div>
                    <!-- // .row -->
                </div>
                <!-- // .container -->
            </div>
            <!-- // .top-header -->

            <div class="main-header">
                <div class="inote">
                     <asp:Label ID="lbl_loginName" runat="server"></asp:Label>
                    您好
					<asp:LinkButton ID="lbtn_logout" runat="server" Text="登出" OnClick="lbtn_logout_Click"  meta:resourcekey="lbtn_logoutResource1"></asp:LinkButton>
                </div>
                <div class="container">

                    <div id="mainmenu2" class="site-menu visible-lg visible-md right-menu">

                        <div class="left-menu hide">
                            <div class="left-menu-info">
                                <form action="post">
                                    <input type="text" placeholder="Type to search" name="s">
                                    <button><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <!-- // .left-menu-info -->
                        </div>
                        <!-- // .left-menu.hide -->

                         <nav>
                            <ul class="menu sf-menu list-unstyled clearfix">
                                <li>
                                    <a href="../WebPage/index.aspx">首頁</a>
                                </li>

                                <li>
                                    <a href="page_about_unit.html">關於我們</a>
                                    <ul>
                                        <li><a href="page_alliance-rule.html">宗旨與服務</a></li>
                                        <li><a href="page_about_mission.html">願景與使命</a></li>
                                        <li><a href="page_about_research.html">研發重點</a></li>
                                        <li><a href="page_alliance-item.html">服務項目</a></li>
                                        <li><a href="page_alliance-join.html">加入會員</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="page_emotion-intro.html">感性設計</a>
                                    <ul>
                                        <li><a href="page_emotion-intro.html">感性設計介紹</a></li>
                                        <li><a href="page_emotion-example.html">應用實例</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="pape_energy-platform.html">技術能量</a>
                                    <ul>
                                        <li><a href="pape_energy-platform.html">感性設計系統平台</a></li>
                                        <li><a href="pape_energy-3D1.html">3D 快速成形</a></li>
                                        <li><a href="pape_energy-3D2.html">3D 光學掃描</a></li>
                                        <li><a href="pape_energy-action.html">無標籤動作攫起</a></li>
                                        <li><a href="pape_energy-pressure.html">接觸式壓力分布量測</a></li>
                                        <li><a href="pape_energy-eyes.html">眼球追蹤結合生理訊號系統</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="page_industry-lab.html">產業服務</a>
                                    <ul>
                                        <li>
                                            <a href="page_industry-lab.html">開放實驗室</a>
                                            <ul>
                                                <li><a href="page_industry-lab01.html">快速成型特色實驗室</a></li>
                                                <li><a href="page_industry-lab02.html">使用者行為</br>分析實驗室</a></li>
                                                <li><a href="page_industry-lab03.html">產品設計分析與</br>模擬實驗室</a></li>
                                                <li><a href="page_industry-lab04.html">創意文閣/</br>設計精品陳列室</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="page_industry-service.html">委託服務</a></li>
                                        <li><a href="page_industry-traning.html">教育訓練</a></li>
                                    </ul>
                                </li>

                                <li><a href="page_contact.html">聯絡我們</a></li>
                                <li><a href="page_news.html">最新消息</a></li>
                                <li><a href="UserLogin.aspx">會員登入</a></li>
                                <li class="current">
                                    <asp:LinkButton ID="lbtn_goToSearch" runat="server" OnClick="lbtn_goToSearch_Click">語意搜尋平台</asp:LinkButton>
                                    <%--<a href="../WebPage/SemanticSearch.aspx?"+Request.Querystring[]>語意搜尋平台</a>--%>
                                </li>
                            </ul>
                        </nav>
                        <!-- // nav -->
                    </div>
                    <!-- // .site-menu.right-menu -->
                    </br></br>

	  <!-- Repsonsive Menu Trigger -->
                    <a class="pull-right responsive-menu visible-sm visible-xs" href="#panel-menu" id="responsive-menu"><i class="fa fa-bars"></i></a>
                    <!-- End Reposnvie Menu Trigger -->

                </div>
                <!-- // .container -->
            </div>
            <!-- // .main-header -->
        </header>
        <!-- // header -->

        <div id="mao" class="mao">
            <p><a href="index.aspx">首頁</a>&nbsp;>&nbsp;<a href="#"> 感性設計系統平台</a>&nbsp;>&nbsp;<a href="#"><span>感性設計意象資料庫 </span></a>&nbsp;>&nbsp;<a name="my_collection"><span class="li">我的收藏 </span></a></p>
            </br>

	<div class="goo">
        <div class="ico">
            <img src="../images/lo.png">
        </div>
        <p class="title">
            &nbsp;&nbsp;感性設計意象資料庫 我的收藏</br><span class="xtit">&nbsp;&nbsp;</span>

        </p>

    </div>



            <!-- 分隔線 -->

            <div class="row">

                <!-- 內容start -->
                <div class="twocol" style="font-size:16px;font-family:微軟正黑體">
                    <span class="font-myblue"> 收藏夾數量：  </span> <asp:Label ID="lbl_cartCount" runat="server" Text="10"></asp:Label>
                                  
                    <span class="font-myblue" style="margin-left:80px" > 收藏圖片總數： </span>  <asp:Label ID="lbl_PicCount" runat="server" Text="68"></asp:Label>
                    <div class="right"><a href="#" class="buttonsty" onclick="goToSearch();return true;">返回前頁</a></div>
                </div>
                <br />
                <div class="margin10TB" >
                    <asp:Label class="DownBarTitle"  runat="server" >【我的收藏】</asp:Label>
                    <asp:Label class="DownGrayBar"  runat="server" >選擇資料夾後，點選右側 "意象分析" </asp:Label>
                </div>
                <br />
                <br />
                <br />
                <!-- twocol -->
                <div class="row">
                    <asp:Repeater ID="rpt_cartContent" runat="server">
                        <ItemTemplate>
                            <div class=" col-sm-4 col-md-4 col-lg-4">
                                 
                                <div style="margin-bottom:10px;font-size:1vw">
                                    
                                    <asp:Label style="float:initial" ID="lbl_cartName" runat="server" Text="收藏夾名稱  "></asp:Label>
                                    <input id="cartName" style="width:11vw;margin-left:3px;margin-right:3px;text-align:center;position:relative" type="text" name="欄位名稱" onchange="updateCartName('<%# DataBinder.Eval(Container.DataItem, "c_id") %>', this.value);return true;" value='<%# DataBinder.Eval(Container.DataItem, "c_name") %>'>

                                    <a style="float: right;" id="deleteCart" href="#" onclick="deleteCart('<%# DataBinder.Eval(Container.DataItem, "c_id") %>');return true;">刪除
                                    </a>
                                </div>
                               <checkbox id="cb_cartCheck" class="shoppingcheck2" style="z-index:1" onclick="checkCart('<%# DataBinder.Eval(Container.DataItem, "c_id")%>');return true;">

                               </checkbox>
                                <%-- <div class="shoppingcheck2"></div>--%>
                                <div class="prolistblock">
                                    <%--<div class="margin10T text-center">
										<img src="../images/shoppingcartbig.png">
									</div>--%>
                                    <%--<div class="margin10TB font-size4">
                                        <%# DataBinder.Eval(Container.DataItem, "c_name")%>
                                    </div>--%>
                                    <div class="row">
                                        <div class=" col-sm-12 col-md-12 col-lg-12 margin5B">
                                            <div class="shoppingcartbigimg margin5T" style="height: 300px" >
                                                <img src="<%# DataBinder.Eval(Container.DataItem, "mainPicUrl")%>" class="img-responsive">
                                            </div>
                                        </div>
                                        <!-- col -->

                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 margin5B">
                                            <div class="shoppingcartsmallimg margin5T">
                                                <%--<img src="<%# DataBinder.Eval(Container.DataItem, "mainPicUrl")%>" class="img-responsive">--%>
                                                <img src="<%# DataBinder.Eval(Container.DataItem, "subPicUrl1")%>" class="img-responsive">
                                            </div>
                                        </div>
                                        <!-- col -->

                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 margin5B">
                                            <div class="shoppingcartsmallimg margin5T">
                                                <img src="<%# DataBinder.Eval(Container.DataItem, "subPicUrl2")%>" class="img-responsive">
                                            </div>
                                        </div>
                                        <!-- col -->

                                        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 margin5B">
                                            <div class="shoppingcartsmallimg margin5T">
                                                <img src="<%# DataBinder.Eval(Container.DataItem, "subPicUrl3")%>" class="img-responsive">
                                            </div>
                                        </div>
                                        <!-- col -->
                                    </div>
                                    <!-- row -->
                                </div>
                                <!-- prolistblock -->
                            </div>
                        </ItemTemplate>

                    </asp:Repeater>
                    <!-- col -->






                    <%--  <div class="text-center" style="font-size: 24px;">
						<a href="#">◀</a>&nbsp;&nbsp;
		<a href="#">▶</a>
					 
					</div>--%>
                </div>
                <div id="analytics" style="height:80px"></div>
                <div class="margin10TB" >
                    <asp:Label class="DownBarTitle"  runat="server" >【欲分析商品】</asp:Label>
                    <asp:Label class="DownGrayBar"  runat="server" >選取您要進行意象分析的產品</asp:Label>
                </div>
                <div> 
                    <br />
                </div>
                <!-- row -->

                <div class="margin10TB" style="padding:10px">
                    <asp:Label ID="lbl_cartContentTitle" runat="server" Text="收藏夾內容" Font-Bold="True" Font-Size="Medium" ForeColor="#993333"></asp:Label>
                     <a style="margin-left:15px" onclick="allPick()" >全選</a>   
                     <a style="margin-left:10px" onclick="allUnPick()">重設</a> 
                     <asp:LinkButton style="float:right;margin-left:10px;padding:10px" runat="server" onclick="lbtn_resetPic_Click" OnClientClick="clearCart()" >返回我的收藏</asp:LinkButton>
                     <asp:LinkButton class="btn btn-blue" style="float:right;"  ID="lbtn_confirmCheck2" runat="server" OnClick="lbtn_confirmCheck2_Click">確定</asp:LinkButton> 
                  
                </div>

                <div id="div_cartUrls" class="prolistblock">
                    <asp:UpdatePanel ID="up_cartContent" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                               <asp:HiddenField id="hf_AllPick_ImgID" runat="server" />
                               <asp:HiddenField id="hf_AllPick_ImgUrl" runat="server" />
                             <div class="row margin15T">

                                <asp:Repeater ID="rpt_cartUrls" runat="server">
                                    <ItemTemplate>

                                        <div  class=" col-xs-4 col-sm-2 col-md-2 col-lg-1" title='<%# DataBinder.Eval(Container.DataItem, "p_review")%>'>
                                            <%--<div style="text-align: right">
                                                <asp:LinkButton ID="lbtn_oriPage" runat="server" OnClientClick="NewWindow();" PostBackUrl='<%# DataBinder.Eval(Container.DataItem, "p_oriUrl")%>'>Link</asp:LinkButton>
                                            </div>--%>

                                            <checkbox id="cb_productImgCheck" class="shoppingcheck3" onclick="checkImg('<%# DataBinder.Eval(Container.DataItem, "p_imgUrl")%>','<%# DataBinder.Eval(Container.DataItem, "p_id")%>');return true;"></checkbox>
                                            <div class="shoppingcartsmallimg margin5T" style="height: 70px">
                                                <img src="<%# DataBinder.Eval(Container.DataItem, "p_imgUrl")%>" class="img-responsive">
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>


                            </div>
                            <!-- row -->
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="lbtn_confirmCheck" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="lbtn_resetPic" EventName="Click" />
                            
                        </Triggers>
                    </asp:UpdatePanel>
                    


                </div>
                <div class="margin10TB">
                     <asp:Label class="DownBarTitle"  runat="server" >【選擇意象】</asp:Label>
                    <asp:Label class="DownGrayBar"  runat="server">請選擇兩項形容詞成為意象分析的軸向</asp:Label>
                </div> 

                <div class="margin10TB padding10TB">
                    <asp:Label ID="lbl_picContentTitle" runat="server" Text="產品意象圖內容" Font-Bold="True" Font-Size="Medium" ForeColor="#993333"></asp:Label>
                    <asp:LinkButton  style="margin-left:10px;font-size:10px" ID="lbtn_resetPic" runat="server" Font-Size="Medium" OnClientClick="allUnPick()" OnClick="lbtn_BackStep_Click">回上一步</asp:LinkButton>
                </div>
                <div id="div_finalCartUrls" class="prolistblock">
                    <asp:UpdatePanel ID="up_finalProduct" runat="server" UpdateMode="Always">
                        <ContentTemplate>

                            <div style="display: table;margin: 0 auto;">
                                <asp:CheckBoxList ID="cbxl_termLeader" style="border-spacing:10px;border-collapse: separate;"  runat="server" RepeatDirection="Horizontal" CellPadding="100" CellSpacing="100" RepeatColumns="13" >

                                </asp:CheckBoxList>
                            </div>

                            <div class="row margin15T">
                                <asp:Repeater ID="rpt_fianlCartUrls" runat="server">
                                    <ItemTemplate>
                                        <div class=" col-xs-4 col-sm-2 col-md-2 col-lg-1">
                                            <div class="shoppingcartsmallimg margin5T" style="height: 70px">

                                                <img src="<%# DataBinder.Eval((System.Collections.Generic.KeyValuePair<string, string[,]>)Container.DataItem,"Key")%>" class="img-responsive">
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="lbtn_confirmCheck2" EventName="Click" />
                            <asp:AsyncPostBackTrigger ControlID="lbtn_resetPic" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>

                </div>
                
                <div class="margin10TB" style="text-align:center" >
                        <asp:LinkButton  CssClass="buttonsty"  ID="btn_ToImagePicture" runat="server" OnClientClick="return GetSelectedItem()" disabled>產生意象圖</asp:LinkButton>
                
                  <%--   <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
						<ContentTemplate>
							<div class="text-center">
								<asp:CheckBoxList ID="cbxl_termLeader" runat="server" RepeatDirection="Horizontal">
								</asp:CheckBoxList>
							</div>
						</ContentTemplate>
						<Triggers>
							<asp:AsyncPostBackTrigger ControlID="lbtn_generatePic" EventName="Click" />
						</Triggers>
					</asp:UpdatePanel>--%>
                    <%--<asp:LinkButton class="buttonfull" ID="lbtn_generatePic" runat="server" OnClientClick="return GetSelectedItem()" OnClick="lbtn_generatePic_Click">產生產品意象圖</asp:LinkButton>--%>
                    <%--<asp:LinkButton class="buttonfull" ID="LinkButton1" runat="server" OnClientClick="return GetSelectedItem()">產生意象圖</asp:LinkButton>--%>
                </div>
                <br>



                <!-- 內容end -->


            </div>
            <!-- // .row -->
            </br>
			<!-- 分隔線 -->





        </div>
        <footer id="footer">

            <div id="footer-1" class="widget-area">
                <div class="container">
                    <div class="row">

                        <div class="widget quick-contact col-lg-9 col-md-9 bottom-30-sm bottom-30-xs">
                            <ul class="footernav">
                                <li>
                                    <a href="page_about_unit.html"><b style="font-size: 15px;">關於我們</b></a> </br>
				<a href="page_alliance-rule.html">宗旨與服務</a></br>
				<a href="page_about_mission.html">願景與使命</a></br>
				<a href="page_about_research.html">研發重點</a></br>
				<a href="page_alliance-item.html">服務項目</a></br>
				<a href="page_alliance-join.html">加入會員</a>
                                </li>
                                <li>
                                    <a href="#"><b style="font-size: 15px;">感性設計</b></a> </br>
			  <a href="page_emotion-intro.html">感性設計介紹</a></br>
			  <a href="page_emotion-example.html">應用實例</a>
                                </li>
                                <li>
                                    <a href="#"><b style="font-size: 15px;">技術能量</b></a> </br>
			  <a href="pape_energy-platform.html">感性設計系統平台</a></br>
			  <a href="pape_energy-3D1.html">3D 快速成形</a></br>
			  <a href="pape_energy-3D2.html">3D 光學掃描</a></br>
			  <a href="pape_energy-action.html">無標籤動作攫起</a></br>
			  <a href="pape_energy-pressure.html">接觸式壓力分布量測</a></br>
			  <a href="pape_energy-eyes.html">眼球追蹤結合生理訊號系統</a>
                                </li>
                                <li>
                                    <a href="#"><b style="font-size: 15px;">產業服務</b></a></br>
			  <a href="page_industry-lab.html">開放實驗室</a></br>
			  <a href="page_industry-service.html">委託服務</a></br>
			  <a href="page_industry-traning.html">教育訓練</a>
                                </li>
                                <li>
                                    <a href="page_contact.html"><b style="font-size: 15px;">聯絡我們</b></a>
                                </li>
                                <li>
                                    <a href="page_news.html"><b style="font-size: 15px;">最新消息</b></a>
                                </li>
                            </ul>
                            <!-- <div class="widget-title">
			<h5>Quick Contact</h5>
		  </div> -->
                            <!-- // .widget-title -->

                            <!-- <p>Amet, commodi, hic fugiat repellat mollitia est nesciunt nostrum error tenetur itaque totam beatae at blanditiis ullam suscipit! Modi, iure accusantium cum?</p>

		  <ul class="list-unstyled contact-field-list clearfix">
			<li><i class="fa fa-home"></i>102, Vancover</li>
			<li><i class="fa fa-envelope"></i>hello@prettyname.com</li>
			<li><i class="fa fa-mobile"></i>(+01) . 234 . 567</li>
			<li><i class="fa fa-phone"></i>1900-500-EROSS</li>
		  </ul> -->
                            <!-- // .contact-field-list -->

                        </div>
                        <!-- // .widget -->

                        <div class="widget col-lg-3 col-md-3 bottom-30-sm bottom-30-xs">
                            <img src="../images/footer-logo.gif" alt="">
                            <p class="top-10" style="color: #BBB;">業務窗口：郝任珍(分機:97878)</br>網站製作：資訊中心｜問題反應｜系統使用說明書｜網站地圖｜</p>
                        </div>
                        <!-- // .widget -->

                    </div>
                    <!-- // .row -->
                </div>
                <!-- // .container -->
            </div>
            <!-- // .widget-area -->

            <div class="credit">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <span class="fed">版權所有 © 2014 工業技術研究院</span>
                        </div>
                    </div>
                    <!-- // .row -->
                </div>
                <!-- // .container -->
            </div>
            <!-- // .credit -->
        </footer>
        <!-- // #footer -->
       
        <div id="ForsearchBlock"  >
             
              <div id="Forsearchbtn" style="margin:3px">
                <asp:LinkButton title="條件搜尋"  runat="server" OnClick="lbtn_goToSearch_Click"> <img width="74px" height="39px" src="../images/btn-search.png"></asp:LinkButton>
            </div>
              <div id="Forsearchbtn" style="margin:3px">
                <a title="我的收藏" onclick="scrollToTop();return true;"> <img width="74px" height="39px" src="../images/btn-my-collection.png"></a>
              </div>
                <div id="Forsearchbtn" style="margin:3px"> 
                    <div class="shoppingcartnumber" style="position:absolute;margin:1px">
                        <asp:Label ID="lbl_productCount" runat="server" Visible="true" Text="0"></asp:Label>
                    </div>
                    <asp:LinkButton title="意象分析"  ID="lbtn_confirmCheck" runat="server" OnClientClick="scrollTo()"  OnClick="lbtn_confirmCheck_Click" disabled> <img width="74px" height="39px" src="../images/btn-analytics.png"></asp:LinkButton>
                 </div>
        </div>

        <!-- colorbox -->
        <div style="display: none;">
            <div id="picblock">

                <img src="../tmpimg/tmpcross.jpg" width="100%">

                <div class="twocol margin10T">
                    <div class="right">
                        <button class="closecolorbox">關閉           </div>
                </div>
                <!-- twocol -->

                <br />
                <br />
            </div>
        </div>

        <!-- Core JavaScript
================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../js/bootstrap.js"></script>
        <script src="../js/easing.js"></script>
        <script src="../js/superfish.js"></script>
        <script src="../js/fitvids.js"></script>
        <script src="../js/flexslider.js"></script>
        <script src="../js/mediaelement.js"></script>
        <script src="../js/isotope.js"></script>
        <script src="../js/easypiechart.js"></script>
        <script src="../js/caroufredsel.js"></script>
        <script src="../js/jpanelmenu.js"></script>
        <script src="../js/magnific.js"></script>
        <script src="../js/twitter/tweet.js"></script>
        <script src="../js/functions.js"></script>
        <!-- Revolutions slider -->
        <script src="../rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <script src="../rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("a[rel='ja-colorbox']").colorbox();
            });
            function changeImage(object, ahref) {
                $("div.product-img-box li").removeClass("active");
                $(object).parent("li").addClass("active");
                $(this).addClass("active");
                $("#pimage").attr("src", object.href);
                $("a[rel='ja-colorbox']").attr("href", ahref);
                return false;
            }
        </script>
        <script type="text/javascript" src="../js/jquery-migrate-1.2.1.js"></script>
        <script type="text/javascript" src="../js/jquery.colorbox.js"></script>
        <script type="text/javascript" src="../js/function.js"></script>

        <script type="text/javascript">
            $(".colorboxGen").colorbox({ inline: true, width: "50%", maxWidth: "900", MaxHeight: "80%", opacity: 0.5 });
            $(".closecolorbox").click(function () {
                $.colorbox.close()
            })
            //點選切換樣式
            $(".shoppingcheck2").click(function () {
                $(this).toggleClass("shoppingcheckup2");
            });
            $(".shoppingcheck3").click(function () {
                $(this).toggleClass("shoppingcheckup3");
            })
        </script>
    </form>
</body>
</html>
