﻿using Registration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebPage_AddUser : System.Web.UI.Page
{
    publicFunction pubFun = new publicFunction();
    User_Model user = new User_Model();
    AddUser_DAL addUser = new AddUser_DAL();
    protected void Page_Load(object sender, EventArgs e)
    {

        UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;

    }

    protected void lbtn_apply_Click(object sender, EventArgs e)
    {
        user.user_id = HttpContext.Current.Request.Cookies["user_id"].Value;
        user.user_st = HttpContext.Current.Request.Cookies["user_st"].Value;
        user.user_name = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["user_name"].Value);
        user.user_tel = HttpContext.Current.Request.Cookies["user_tel"].Value;
        user.user_comName = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["user_comName"].Value);
        user.user_comNo = HttpContext.Current.Request.Cookies["user_comNo"].Value;
        user.user_address = HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["user_address"].Value);



        if (addUser.InsertIntoUser(user) == 0)
        {
            Registration.JavaScript.AlertMessage(this.Page, "帳號已存在，註冊失敗!");
            Response.Write("<script>window.parent.location='" + "../WebPage/AddUser.aspx" + "'</script>");
        }
        else
        {
            Session["user_id"] = user.user_id;
            Registration.JavaScript.AlertMessage(this.Page, "註冊成功!");
            Response.Write("<script>window.parent.location='" + "../WebPage/SemanticSearch.aspx?loginName=" + user.user_id + "'</script>");
        }

    }


    #region 驗證


    protected void CheckPwAgain(object sender, ServerValidateEventArgs args)
    {
        if (account_st.Text.Trim() != "")
        {
            if (account_st.Text.Trim() != account_stv.Text.Trim())
                args.IsValid = false;
            else
                args.IsValid = true;
        }
    }
    #endregion

    /// <summary>
    /// 去搜尋_20161103_howard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_goToSearch_Click(object sender, EventArgs e)
    {
        Response.Write("<script>window.parent.location='" + "../WebPage/SemanticSearch.aspx?loginName=" + lbl_loginName.Text + "'</script>");
    }
    /// <summary>
    /// 登出_20161103_howard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_logout_Click(object sender, EventArgs e)
    {

        Session["user_id"] = "";
        Session["user_st"] = "";
        Response.Write("<script>window.parent.location='" + "../WebPage/index.aspx" + "'</script>");
        lbtn_logout.Visible = false;

    }
}








