﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

public partial class WebPage_ImagePicture : System.Web.UI.Page
{
    publicFunction pubFun = new publicFunction();
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            //string[] finalImgIdString = Regex.Split(HttpContext.Current.Request.Cookies["finalImgIdString"].Value, "_id_", RegexOptions.IgnoreCase);
            string[] finalTermLeaderString = Regex.Split(HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies["finalTermLeaderString"].Value), "_term_", RegexOptions.IgnoreCase);
            lbl_xyName.Text = "意象詞: " + finalTermLeaderString[0]+" and "+ finalTermLeaderString[1]; 
            string s = string.Format(@"initMatrix('{0}','{1}','{2}');", "../Handler/GetProductInfo.ashx?finalImgIdString=" + HttpContext.Current.Request.Cookies["finalImgIdString"].Value+ "&finalTermLeaderString=" + HttpContext.Current.Request.Cookies["finalTermLeaderString"].Value, finalTermLeaderString[0], finalTermLeaderString[1]);
            ClientScript.RegisterStartupScript(this.GetType(), "script", s, true);
        }
    }


}