﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class WebPage_UserLogin : System.Web.UI.Page
{
    publicFunction pubFun = new publicFunction();
    User_Model user = new User_Model();
    UserLogin_DAL userLogin = new UserLogin_DAL();
    protected void Page_Load(object sender, EventArgs e)
    {

        lbl_loginName.Text = Convert.ToString(Session["user_id"]);
        if (lbl_loginName.Text == "")
        {
            lbtn_logout.Visible = false;
        }
        else
        {
            lbtn_logout.Visible = true;
        }
        UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
    }
    protected void lbtn_login_Click(object sender, EventArgs e)
    {

        //if (!Page.ClientScript.IsStartupScriptRegistered("clicktest"))
        //{
        //    ScriptManager.RegisterStartupScript(Page, GetType(), "clicktest", "<script>pdCookie()</script>", false);
        //}
        lbtn_login.Attributes.Add("onclick", string.Format("idCookie();return false;"));
        lbtn_login.Attributes.Add("onclick", string.Format("pdCookie();return false;"));
        Session["user_id"] = HttpContext.Current.Request.Cookies["user_id"].Value;
        if (HttpContext.Current.Request.Cookies["user_st"] != null)
            Session["user_st"] = HttpContext.Current.Request.Cookies["user_st"].Value; 
        //Session["user_id"] = user_id.Value;
        //Session["user_st"] = user_st.Value;
        user.user_id = Convert.ToString(Session["user_id"]);
        user.user_st= Convert.ToString(Session["user_st"]);
        if(userLogin.selectUser(user)==1)//登入成功
        {
            Response.Write("<script>alert('登入成功！')</script>");
            Response.Write("<script>window.parent.location='" + "../WebPage/SemanticSearch.aspx?loginName=" + user.user_id + "'</script>");
        }
        else//登入失敗，
        {
            Response.Write("<script>alert('登入失敗！')</script>");
            Session["user_id"] = "";
            Session["user_st"] = "";
            Response.Write("<script>window.parent.location='" + "../WebPage/index.aspx" + "'</script>");
            lbtn_logout.Visible = false;
        }

    }
    /// <summary>
    /// 去搜尋_20161103_howard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_goToSearch_Click(object sender, EventArgs e)
    {
        Response.Write("<script>window.parent.location='" + "../WebPage/SemanticSearch.aspx?loginName=" + lbl_loginName.Text + "'</script>");
    }
    /// <summary>
    /// 登出_20161103_howard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_logout_Click(object sender, EventArgs e)
    {

        Session["user_id"] = "";
        Session["user_st"] = "";
        Response.Write("<script>window.parent.location='" + "../WebPage/index.aspx" + "'</script>");
        lbtn_logout.Visible = false;

    }
}