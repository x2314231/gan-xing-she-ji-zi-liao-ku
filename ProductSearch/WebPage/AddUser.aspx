﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddUser.aspx.cs"  EnableEventValidation="false" Inherits="WebPage_AddUser" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="../css/bootstrap.css" rel="stylesheet" />

    <!-- jQuery load -->
    <script src="../js/jquery.min.js"></script>

    <!-- Custom styles for this template -->
    <link href="../css/style.css" rel="stylesheet" />
    <link href="../css/plugin.css" rel="stylesheet" />
    <link href="../css/font-awesome.min.css" rel="stylesheet" />
    <link href="../css/animate.min.css" rel="stylesheet" />
    <link href="../css/responsive.css" rel="stylesheet" />

    <!-- Revolutions slider -->
    <link href="../rs-plugin/css/settings.css" rel="stylesheet" />
    <link href="../rs-plugin/css/captions.css" rel="stylesheet" />

    <link href="../css/myITRIproject/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" />


    <script type="text/javascript">

        function vCookie() {
            
            document.cookie = "user_id=" + document.getElementById('<%=account_ActAdd.ClientID %>').value;
            document.cookie = "user_st=" + document.getElementById('<%=account_st.ClientID %>').value;
            
            document.cookie = "user_tel=" + document.getElementById('<%=account_tel.ClientID %>').value;
          
            document.cookie = "user_comNo=" + document.getElementById('<%=account_company_no.ClientID %>').value;
            

          

            var users = document.getElementById('<%=account_name.ClientID %>').value;
            var userlogin = encodeURI(users);
            document.cookie = "user_name=" + userlogin;


            var com = document.getElementById('<%=account_company.ClientID %>').value;
            var usercom = encodeURI(com);
            document.cookie = "user_comName=" + usercom;

            var address = document.getElementById('<%=account_address.ClientID %>').value;
            var useraddress = encodeURI(address);
            document.cookie = "user_address=" + useraddress;
        };

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="rev_slider_wrapper fullscreen-container">
            <div id="rev_slider" class="rev_slider fullscreenbanner" style="overflow: visible; height: 250px !important;">
                <ul>
                    <!-- SLIDE  -->
                    <li data-transition="random" data-slotamount="7" data-masterspeed="300">
                        <!-- MAIN IMAGE -->
                        <img src="../images/sliderbg.jpg" alt="slide1_background" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption sfb" data-x="450" data-y="0" data-speed="300" data-start="500" data-easing="easeOutBounce" data-endspeed="300" style="z-index: 2">
                            <img src="../images/slider1_1.png" alt="">
                        </div>

                    </li>
                    <!-- SLIDE  -->
                    <li data-transition="random" data-slotamount="7" data-masterspeed="300">
                        <!-- MAIN IMAGE -->
                        <img src="../images/sliderbg2.jpg" alt="black_linen_v2" data-bgfit="normal" data-bgposition="center top" data-bgrepeat="repeat">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption sfb" data-x="400" data-y="0" data-speed="300" data-start="500" data-easing="easeOutBounce" data-endspeed="300" style="z-index: 2">
                            <img src="../images/slider1_1.png" alt="">
                        </div>
                    </li>
                    <li data-transition="random" data-slotamount="7" data-masterspeed="300">
                        <!-- MAIN IMAGE -->
                        <img src="../images/sliderbg3.jpg" alt="black_linen_v2" data-bgfit="normal" data-bgposition="center top" data-bgrepeat="repeat">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption sfb" data-x="400" data-y="0" data-speed="300" data-start="500" data-easing="easeOutBounce" data-endspeed="300" style="z-index: 2">
                            <img src="../images/slider1_1.png" alt="">
                        </div>
                    </li>
            </div>
            </li>
        </ul>
        </div>
        <script type="text/javascript">
            var revapi1;
            jQuery(document).ready(function () {

                if (jQuery.fn.cssOriginal != undefined)
                    jQuery.fn.css = jQuery.fn.cssOriginal;

                if (jQuery('#rev_slider').revolution == undefined)
                    revslider_showDoubleJqueryError('#rev_slider');
                else
                    revapi1 = jQuery('#rev_slider').show().revolution(
                    {
                        delay: 9000,
                        startwidth: 1170,
                        startheight: 250,
                        hideThumbs: 200,

                        thumbWidth: 100,
                        thumbHeight: 50,
                        thumbAmount: 3,

                        navigationType: "bullet",
                        navigationArrows: "solo",
                        navigationStyle: "round",

                        touchenabled: "on",
                        onHoverStop: "on",

                        navigationHAlign: "center",
                        navigationVAlign: "bottom",
                        navigationHOffset: 0,
                        navigationVOffset: 20,

                        soloArrowLeftHalign: "left",
                        soloArrowLeftValign: "center",
                        soloArrowLeftHOffset: 20,
                        soloArrowLeftVOffset: 0,

                        soloArrowRightHalign: "right",
                        soloArrowRightValign: "center",
                        soloArrowRightHOffset: 20,
                        soloArrowRightVOffset: 0,

                        shadow: 0,
                        fullWidth: "on",
                        fullScreen: "off",

                        stopLoop: "off",
                        stopAfterLoops: -1,
                        stopAtSlide: -1,

                        shuffle: "off",

                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        startWithSlide: 0,
                        videoJsPath: "../js/revslider/js/videojs/",
                        fullScreenOffsetContainer: ""
                    });

            }); //ready
        </script>
        <header id="header">
            <div class="top-header">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-12 col-md-12 text-right top-header-right">

                            <div class="header-search pull-right visible-lg visible-md" id="header-search">
                                <form method="get" id="searchform" action="#">
                                    <div class="header-search-input-wrap">
                                        <input class="header-search-input" placeholder="Type to search..." type="text" value="" name="s" id="s" />
                                    </div>
                                    <input class="header-search-submit" type="submit" id="go" value=""><span class="header-icon-search"><i class="fa fa-search"></i></span>
                                </form>
                                <!-- // #searchform -->
                            </div>
                            <!-- // .header-search -->

                            <ul class="social list-unstyled pull-right">
                                <li class="blogger"><a data-toggle="tooltip" data-placement="bottom" title="Blogger Page" href="#"><span><b>B</b></span></a></li>
                                <li class="facebook"><a data-toggle="tooltip" data-placement="bottom" title="Facebook Page" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="twitter"><a data-toggle="tooltip" data-placement="bottom" title="Follow us on Twitter" href="#"><i class="fa fa-twitter"></i></a></li>

                            </ul>
                            <!-- // .social -->

                        </div>
                        <!-- // .top-header-right -->

                    </div>
                    <!-- // .row -->
                </div>
                <!-- // .container -->
            </div>
            <!-- // .top-header -->

            <div class="main-header">
                <div class="inote">
                    <asp:Label ID="lbl_loginName" runat="server"></asp:Label>
                    您好
                    <asp:LinkButton ID="lbtn_logout" runat="server" Text="登出" meta:resourcekey="lbtn_logoutResource1" OnClick="lbtn_logout_Click"></asp:LinkButton>
                </div>
                <div class="container">

                    <div id="mainmenu2" class="site-menu visible-lg visible-md right-menu">

                        <div class="left-menu hide">
                            <div class="left-menu-info">
                                <form action="post">
                                    <input type="text" placeholder="Type to search" name="s">
                                    <button><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <!-- // .left-menu-info -->
                        </div>
                        <!-- // .left-menu.hide -->

                        <nav>
                            <ul class="menu sf-menu list-unstyled clearfix">
                                <li>
                                    <a href="../WebPage/index.aspx">首頁</a>
                                </li>

                                <li>
                                    <a href="page_about_unit.html">關於我們</a>
                                    <ul>
                                        <li><a href="page_alliance-rule.html">宗旨與服務</a></li>
                                        <li><a href="page_about_mission.html">願景與使命</a></li>
                                        <li><a href="page_about_research.html">研發重點</a></li>
                                        <li><a href="page_alliance-item.html">服務項目</a></li>
                                        <li><a href="page_alliance-join.html">加入會員</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="page_emotion-intro.html">感性設計</a>
                                    <ul>
                                        <li><a href="page_emotion-intro.html">感性設計介紹</a></li>
                                        <li><a href="page_emotion-example.html">應用實例</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="pape_energy-platform.html">技術能量</a>
                                    <ul>
                                        <li><a href="pape_energy-platform.html">感性設計系統平台</a></li>
                                        <li><a href="pape_energy-3D1.html">3D 快速成形</a></li>
                                        <li><a href="pape_energy-3D2.html">3D 光學掃描</a></li>
                                        <li><a href="pape_energy-action.html">無標籤動作攫起</a></li>
                                        <li><a href="pape_energy-pressure.html">接觸式壓力分布量測</a></li>
                                        <li><a href="pape_energy-eyes.html">眼球追蹤結合生理訊號系統</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="page_industry-lab.html">產業服務</a>
                                    <ul>
                                        <li>
                                            <a href="page_industry-lab.html">開放實驗室</a>
                                            <ul>
                                                <li><a href="page_industry-lab01.html">快速成型特色實驗室</a></li>
                                                <li><a href="page_industry-lab02.html">使用者行為</br>分析實驗室</a></li>
                                                <li><a href="page_industry-lab03.html">產品設計分析與</br>模擬實驗室</a></li>
                                                <li><a href="page_industry-lab04.html">創意文閣/</br>設計精品陳列室</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="page_industry-service.html">委託服務</a></li>
                                        <li><a href="page_industry-traning.html">教育訓練</a></li>
                                    </ul>
                                </li>

                                <li><a href="page_contact.html">聯絡我們</a></li>
                                <li><a href="page_news.html">最新消息</a></li>
                                <li><a href="UserLogin.aspx">會員登入</a></li>
                                <li class="current">
                                    <asp:LinkButton ID="lbtn_goToSearch" runat="server" OnClick="lbtn_goToSearch_Click">語意搜尋平台</asp:LinkButton>
                                    <%--<a href="../WebPage/SemanticSearch.aspx?"+Request.Querystring[]>語意搜尋平台</a>--%>
                                </li>
                            </ul>
                        </nav>
                        <!-- // nav -->
                    </div>
                    <!-- // .site-menu.right-menu -->
                    </br></br>

                <!-- Repsonsive Menu Trigger -->
                    <a class="pull-right responsive-menu visible-sm visible-xs" href="#panel-menu" id="responsive-menu"><i class="fa fa-bars"></i></a>
                    <!-- End Reposnvie Menu Trigger -->

                </div>
                <!-- // .container -->
            </div>
            <!-- // .main-header -->
        </header>



        <div class=" stripeMe font-gray">

            <table style="width: 90%; margin: 0 auto">
                <tr>
                    <td style="border: 0"><span style="color: Red">*為必填</span></td>
                </tr>
                <tr>
                    <th style="width: 10%; text-align: right; font-size: 14px">
                        <a style="color: Red">*</a>帳號(email):
                    </th>
                    <td>
                        <asp:TextBox ID="account_ActAdd"  runat="server" Width="50%" MaxLength="150"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="account_ActAdd" ForeColor="#cc0000" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                       <%-- <asp:CustomValidator ID="cuv_CheckMail" runat="server" ControlToValidate="account_ActAdd" OnServerValidate="Checkemail"
                            Text="電子信箱已重複" ForeColor="#CC0000" ErrorMessage="電子信箱已重複" ValidateEmptyText="True"></asp:CustomValidator>--%>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="account_ActAdd" ForeColor="#CC0000" Text="電子信箱格式錯誤" ErrorMessage="格式錯誤" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <th style="width: 15%; text-align: right; font-size: 14px">
                        <a style="color: Red">*</a>密碼:
                    </th>
                    <td>
                        <asp:TextBox ID="account_st"  runat="server" TextMode="Password" Width="50%" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="account_st" ForeColor="#cc0000" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>

                </tr>
                <tr>
                    <th style="width: 15%; text-align: right; font-size: 14px">
                        <a style="color: Red">*</a>密碼確認:
                    </th>
                    <td>
                        <asp:TextBox ID="account_stv" runat="server" TextMode="Password" Width="50%" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="account_stv" ForeColor="#cc0000" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="account_stv" OnServerValidate="CheckPwAgain"
                            Text="密碼確認錯誤" ForeColor="#CC0000" ErrorMessage="密碼確認錯誤" ValidateEmptyText="True"></asp:CustomValidator>
                    </td>


                </tr>

                <tr>
                    <th style="width: 22%; text-align: right; font-size: 14px">
                        <a style="color: Red">*</a>姓名:
                    </th>
                    <td>
                        <asp:TextBox ID="account_name"  runat="server" Width="50%" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="account_name" ForeColor="#cc0000" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>


                </tr>
                <tr>
                    <th style="width: 15%; text-align: right; font-size: 14px">
                        <a style="color: Red">*</a>連絡電話:
                    </th>
                    <td>
                        <asp:TextBox ID="account_tel" runat="server" Width="50%" MaxLength="20"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="account_tel" ForeColor="#cc0000" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>


                </tr>

                <tr>
                    <th style="width: 15%; text-align: right; font-size: 14px">
                        <a style="color: Red">*</a>公司名稱:
                    </th>
                    <td>
                        <asp:TextBox ID="account_company" runat="server" Width="50%" MaxLength="150"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="account_company" ForeColor="#cc0000" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>

                </tr>
                <tr>

                    <th style="width: 15%; text-align: right; font-size: 14px">
                        <a style="color: Red">*</a>公司統編:
                    </th>
                    <td>
                        <asp:TextBox ID="account_company_no"  runat="server" Width="50%" MaxLength="8"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="account_company_no" ForeColor="#cc0000" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                       <%-- <asp:CustomValidator ID="cuv_CheckCompanyNo" runat="server" ControlToValidate="account_company_no" OnServerValidate="CheckCompanyNo"
                            Text="統一編號錯誤" ForeColor="#CC0000" ErrorMessage="統一編號錯誤" ValidateEmptyText="True"></asp:CustomValidator>--%>
                    </td>

                </tr>
                <tr>
                    <th style="width: 15%; text-align: right; font-size: 14px">
                        <a style="color: Red">*</a>公司地址:
                    </th>
                    <td colspan="3">
                        <asp:TextBox ID="account_address" onchange="vCookie();return true;" runat="server" Width="50%" MaxLength="8"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="account_address" ForeColor="#cc0000" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                    </td>
                </tr>

                <%--            <tr>
                <td colspan="4" style="text-align:right" >


                   
                    
                </td>
            </tr>--%>
            </table>

        </div>
        <div style="text-align: right; margin-left: 630px">
            <table>
                <tr>
                   <%-- <td style="border: 0;">
                        <img src="../WabPage/ValidateCode.aspx" id="Validate_Code" runat="server" /></td>
                    <td style="border: 0;">
                        <asp:TextBox ID="ValidateTex" runat="server" onchange="vCookie();return true;" Style="position: static; margin: auto" Width="94px" TabIndex="85" placeholder="請輸入驗證碼"></asp:TextBox></td>--%>
                    <td style="border: 0;">
                        <asp:LinkButton ID="lbtn_apply" runat="server" OnClick="lbtn_apply_Click">送出申請</asp:LinkButton>
                        <%--<asp:Button ID="Save" Text="會員申請" runat="server" OnClick="Save_Click" />--%><%--&nbsp;&nbsp;<input type="button" id="Cancel" value="取消" onclick="CancelClick()" /></td>--%>
                </tr>
            </table>
        </div>

        <script>
            function CancelClick() {
                window.location = "../WebPage/index.aspx"
            }
        </script>



        <footer id="footer">
            <div id="footer-1" class="widget-area">
                <div class="container">
                    <div class="row">

                        <div class="widget quick-contact col-lg-9 col-md-9 bottom-30-sm bottom-30-xs">
                            <ul class="footernav">
                                <li>
                                    <a href="page_about_unit.html"><b style="font-size: 15px;">關於我們</b></a> </br>
                                <a href="page_alliance-rule.html">宗旨與服務</a></br>
                                <a href="page_about_mission.html">願景與使命</a></br>
                                <a href="page_about_research.html">研發重點</a></br>
                                <a href="page_alliance-item.html">服務項目</a></br>
                                <a href="page_alliance-join.html">加入會員</a>
                                </li>
                                <li>
                                    <a href="#"><b style="font-size: 15px;">感性設計</b></a> </br>
                                <a href="page_emotion-intro.html">感性設計介紹</a></br>
                                <a href="page_emotion-example.html">應用實例</a>
                                </li>
                                <li>
                                    <a href="#"><b style="font-size: 15px;">技術能量</b></a> </br>
                                <a href="pape_energy-platform.html">感性設計系統平台</a></br>
                                <a href="pape_energy-3D1.html">3D 快速成形</a></br>
                                <a href="pape_energy-3D2.html">3D 光學掃描</a></br>
                                <a href="pape_energy-action.html">無標籤動作攫起</a></br>
                                <a href="pape_energy-pressure.html">接觸式壓力分布量測</a></br>
                                <a href="pape_energy-eyes.html">眼球追蹤結合生理訊號系統</a>
                                </li>
                                <li>
                                    <a href="#"><b style="font-size: 15px;">產業服務</b></a></br>
                                <a href="page_industry-lab.html">開放實驗室</a></br>
                                <a href="page_industry-service.html">委託服務</a></br>
                                <a href="page_industry-traning.html">教育訓練</a>
                                </li>
                                <li>
                                    <a href="page_contact.html"><b style="font-size: 15px;">聯絡我們</b></a>
                                </li>
                                <li>
                                    <a href="page_news.html"><b style="font-size: 15px;">最新消息</b></a>
                                </li>
                            </ul>
                        </div>
                        <!-- // .widget -->

                        <div class="widget col-lg-3 col-md-3 bottom-30-sm bottom-30-xs">
                            <img src="../images/footer-logo.gif" alt="">
                            <p class="top-10" style="color: #BBB;">業務窗口：郝任珍(分機:97878)</br>網站製作：資訊中心｜問題反應｜系統使用說明書｜網站地圖｜</p>
                        </div>
                        <!-- // .widget -->

                    </div>
                    <!-- // .row -->
                </div>
                <!-- // .container -->
            </div>
            <!-- // .widget-area -->

            <div class="credit">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <span class="fed">版權所有 © 2014 工業技術研究院</span>
                        </div>
                    </div>
                    <!-- // .row -->
                </div>
                <!-- // .container -->
            </div>
            <!-- // .credit -->
        </footer>
        <!-- Core JavaScript
================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../js/bootstrap.js"></script>
        <script src="../js/easing.js"></script>
        <script src="../js/superfish.js"></script>
        <script src="../js/fitvids.js"></script>
        <script src="../js/flexslider.js"></script>
        <script src="../js/mediaelement.js"></script>
        <script src="../js/isotope.js"></script>
        <script src="../js/easypiechart.js"></script>
        <script src="../js/caroufredsel.js"></script>
        <script src="../js/jpanelmenu.js"></script>
        <script src="../js/magnific.js"></script>
        <script src="../js/twitter/tweet.js"></script>
        <script src="../js/functions.js"></script>

        <!-- Revolutions slider -->
        <script src="../rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <script src="../rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
    </form>
</body>
</html>
