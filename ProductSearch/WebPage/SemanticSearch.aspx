﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SemanticSearch.aspx.cs" Inherits="WebPage_SemanticSearch" EnableEventValidation="false" meta:resourcekey="PageResource1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">


    <!-- Bootstrap core CSS -->
    <!--<link href="../css/bootstrap.css" rel="stylesheet"> -->
    <link href="../dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- jQuery load -->
    <script src="../js/jquery.min.js"></script>

    <!-- Custom styles for this template -->
    <link href="../style.css" rel="stylesheet">
    <link href="../css/plugin.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">
    <link href="../css/emotion_search.css" rel="stylesheet">
    <link href="../css/emotion_addcollection.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/colorbox.css" media="all" />
    <link href="../css/emotion_search.css" rel="stylesheet">

    <!-- Revolutions slider -->

    <link href="../rs-plugin/css/settings.css" rel="stylesheet">
    <link href="../rs-plugin/css/captions.css" rel="stylesheet">
    <!-- multi selector -->
    <link href="../css/multiple-select.css" rel="stylesheet">
    <link href="../css/jquery.powertip.css" rel="stylesheet" type="text/css" />


    <!-- 160225 add start -->
    <link href="../css/dcaccordion.css" rel="stylesheet">
    <link href="../css/skins/grey.css" rel="stylesheet" type="text/css" />
    <script type='text/javascript' src='../js/jquery.checkall.js'></script>
    <script type='text/javascript' src='../js/jquery.cookie.js'></script>
    <script type="text/javascript" src="../js/jquery.hoverIntent.minified.js"></script>
    <script type="text/javascript" src="../js/jquery.dcjqaccordion.2.7.min.js"></script>
    <script type="text/javascript" src="../js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="../dist/js/bootstrap.min.js"></script>

    <!-- slider plugin => http://seiyria.com/bootstrap-slider/ -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.7.2/bootstrap-slider.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/9.7.2/css/bootstrap-slider.min.css" rel="stylesheet">

    <style> 
        .DownBlueBar { 
            background: #b0b0b0;
            color: #fff;
            width: 100%;
            padding: 10px;
            display: inline-block;
            text-align: center;
            font-size: 16px;
            text-decoration: none;
        }
        .DownGrayBar { 
            background: #C0C0C0;
            color: #fff;
            width: 100%;
            padding: 10px;
            display: inline-block;
            text-align: center;
            font-size: 16px;
            text-decoration: none;
        }
        .DownBarTitle { 
            float:right;
            color: #fff; 
            font-size: 18px; 
            text-align: center; 
            padding: 10px;
            position:absolute;
        }
    </style>

    <script type="text/javascript"> 
        $(document).ready(function () {
            //checkall
            $('.check-all').checkAll();

            $('.accordion-1').dcAccordion({
                eventType: 'click',
                autoClose: false,
                saveState: false,
                disableLink: true,
                showCount: true,
                speed: 'slow'
            });
            /* 頁籤式選單:若單頁出現兩個以上,需再加入ID */
            $(".tabmenuT2Btn").click(function () {
                $(".tabmenuT2Btn").removeClass("tabmenuT2BtnCurrent");
                $(this).addClass("tabmenuT2BtnCurrent");
            });
            
            //清除舊cookie~避免沒選任何物件按下收藏會直接將上次的選擇結果存進收藏
            document.cookie="imgUrlString=";
        });
    </script>
    <%-- 載完圖片才製作瀑布流_20161006_howard --%>
    <script type="text/javascript">
       
        jQuery(window).on('load', function(){
            //瀑布流
            $('.grid').masonry({
                // options...
                itemSelector: '.grid-item',
                columnWidth: 250
            });

            //powertip:tooltip
            $('.itemhint').powerTip({ placement: 'e', smartPlacement: true, });

            //瀑布法END
        });
    </script>
    <!-- 160225 add end -->


    <%-- 點選搜尋條件_20161006_howard --%>
    <script type="text/javascript">
        function autoclick() {
            var str = <%= new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(Request.QueryString["Query"]) %>;
            if (str==null && isPostBack == false ) {
                lnk = document.getElementById("lbtn_goToSearch");
                lnk.click();
            }
        }
    </script>

    <%-- 開啟視窗_20161006_howard --%>
    <script type="text/javascript">
        function OpenWin(ar_title, url) {
            var URL = url;
            $.colorbox(
            {
                href: URL,
                iframe: true,
                width: "900",
                height: "40%",
                transition: "elastic",
                opacity: "0.5",
                innerWidth: "100%",
                onClosed: function () { },
                overlayClose: false,
                title: ar_title
            });
        }
    </script>


    <%-- 點產品更新購物車數量_20161007_howard --%>
    <script type="text/javascript">
        var imgUrlString ='';

        function productCount(imgUrl) {
            var tempCount= parseInt(document.getElementById("lbl_productCount").textContent);
            
            //判斷圖片網址字串是否已經存在
            //若否，則購物車數量+1，網址新增
            //若是，則購物車數量-1，網址取代為空      
            if(imgUrlString.search(imgUrl)==-1)
            {       
                document.getElementById('<%=lbl_productCount.ClientID %>').textContent=tempCount+1;
                imgUrlString=imgUrlString+'_url_'+imgUrl;
                document.cookie="imgUrlString="+imgUrlString;

            }
            else{         
               
                document.getElementById('<%=lbl_productCount.ClientID %>').textContent=tempCount-1;
                imgUrlString=imgUrlString.replace('_url_'+imgUrl,"");
                document.cookie="imgUrlString="+imgUrlString;
            }

            if(imgUrlString == '')
                document.getElementById("lbtn_add_favorite").setAttribute('disabled','disabled');
            else
                document.getElementById("lbtn_add_favorite").removeAttribute('disabled');

        }
    </script>

    <%-- 導向購物車內容頁面_20161017_howard --%>
    <script type="text/javascript">
        function goToCart() {
            window.location = "ProductCart.aspx?loginName=" + document.getElementById("lbl_loginName").textContent;
        }

        
    </script>

    <%-- linkbutton 另開視窗_20161024_howard --%>
    <script type="text/javascript">
        function NewWindow() {
            document.forms[0].target = "_blank";
        }
    </script>

    <%-- 價位的SlideBar事件 --%>
    <script type="text/javascript">
        //html 頁面已 ready
        $(document).ready(function(){
            // slider init & setting change event
            $("#price_slider").slider({});
            $("#price_slider").on('change', function(e) {
                $("#select_price").text(e.value.newValue.toString().replace(",", " ~ "));
                document.getElementById("hf_select_price").value = e.value.newValue.toString();     
            });  
            var lower = parseInt($.cookie("PriceLower") == null ? "0":$.cookie("PriceLower"));
            var upper = parseInt($.cookie("PriceUpper") == null ? "100000":$.cookie("PriceUpper"));  


            $("#price_slider").slider('setValue',[lower,upper]);
            $("#select_price").text(lower+" ~ "+upper);   
            document.getElementById("hf_select_price").value = lower+","+upper;    

        });
 
    </script>
    <%-- 顯示loading --%>
    <script type="text/javascript">
        function showLoading() {
            document.getElementById('div_loading').style.display = 'block';  
             //順便SET價位的cookie
            var price = document.getElementById("hf_select_price").value.split(",");
            $.cookie('PriceLower', price[0]);
            $.cookie('PriceUpper', price[1]);  
        }
    </script>
     <script type="text/javascript"> 
         function clearAll(){ 
             
             document.getElementById('<%=tbx_query.ClientID %>').value = "";
             $('input[name="AreaMenu"][value="rbtn_area_all"]').prop('checked', true);
             $('input[name="LocationMenu"][value="rbtn_location_all"]').prop('checked', true);
             $('input[name="LangMenu"][value="rbtn_lang_all"]').prop('checked', true);
             $("#price_slider").slider('setValue',[0,100000]);
             $("#select_price").text(0+" ~ "+100000);   
             document.getElementById("hf_select_price").value = 0+","+100000;

             $.cookie('Query',  null, { path: '/' }); 
             $.cookie('AreaMenu',  null, { path: '/' }); 
             $.cookie('LocationMenu',  null, { path: '/' });
             $.cookie('PriceLower',"0");
             $.cookie('PriceUpper',"100000");
             $.cookie('lang',  null, { path: '/' }); 
              
         }
         function MyCollectionClick(){  
             
             $('input[name="CollectionAddWay"][value="rbtn_AddOld"]').prop('checked', true);
         }
         function NewCollectionClick(){  
             
             $('input[name="CollectionAddWay"][value="rbtn_AddNew"]').prop('checked', true);
         }
    </script>


<style> 
.loader {
    border: 4px solid #f3f3f3; /* Light grey */
    border-top: 4px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 30px;
    height: 30px;
    animation: spin 2s linear infinite;
    position: relative;
    left:35%; 

}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}
.loading_span{ 
    color:white;
    font-size:20px;  
}
.loading_div{

    position: absolute;
    left:45%;
    top:45%;  
}
 
.btn_original_link{
    border-radius:5px; 
    background:#999999;
    color:white;
    position:relative;
    z-index:1; 
    padding:5px;
}
.btn_original_link:focus{
    color:white;
}
.span_lang {
    
    float:right; 
    padding:2px 5px 2px 5px; 
     border-radius:5px;
     background:#73AD21;
     color:white
             
}
.Space label
{
   margin-left: 8px;
}
</style>

</head>
<body onload="autoclick()" >
    <form id="SearchPage" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager> 

        <!-- 搜尋互動視窗 -->
           <asp:Panel ID="Panel1" runat="server" DefaultButton="LinkButton2">
                                    
        <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            
        <div id="div_loading" style="width:100%;height:100%; background-color:rgba(0, 0, 0, 0.44);position:absolute;z-index:1;display:none">
            <div class="loading_div">
             <div  class="loader"></div>
                <br />
             <span class="loading_span">Searching...</span>
            </div>
            
        </div>
            <div class="modal-dialog modal-lg sc-modal-dialog">
                <%--<ul class="nav nav-pills sc-nav">
                    <li role="presentation" class="search-tab">
                        <img src="../images/search_tab.png">
                    </li>
                    <li role="presentation" class="collection_tab" onclick="gotocart();return true;">
                        <img src="../images/my_collection_tab.png">
                    </li>
                </ul>--%>
                <div class="modal-content"> 
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><spanaria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <div class="search-modal-header-left"><span class="search-modal-title">條件搜尋</span></div>
		        	    <div class="search-modal-header-right">
		        		    <button type="button" class="btn btn-link btn-log search-modal-go-colletion" onclick="goToCart();return true;">前往我的收藏 &gt;&gt;</button>	
		        	    </div>
		      	    </div>
                    <div class="modal-body ">
                        <div class="container-fluid"> 
                                <div class="row form-horizontal" role="form">
                                        <div class="form-group">
                                            <label for="txt_search_word" class="col-md-3 control-label search-modal-label-area">
                                                搜尋條件 :
                                            </label>
                                            <div class="col-md-6 search-modal-input-area">
                                                  <asp:TextBox ID="tbx_query" class="form-control"  runat="server" meta:resourcekey="tbx_queryResource1" placeholder="(ex. 好用的剪刀)"></asp:TextBox>

                                            </div>
                                            <div class="col-md-3 search-modal-btn-area">
                                                <asp:LinkButton ID="LinkButton2" class="btn btn-sm sc-btn-search" runat="server" Text="搜尋" OnClick="lbtn_search_OnClick" OnClientClick="showLoading()" meta:resourcekey="lbtn_searchResource1" ></asp:LinkButton>
                                                <button type="button" class="btn btn-sm sc-btn-cancel"  onclick="clearAll()">重設</button>
                                            </div>
                                        </div>
                                </div> 
                            <div class="row">
                                <div class="col-md-1 search-left-section-space"></div>
                                <div class="col-md-6 search-border search-left-section">
                                    <span class="search-modal-section-title"><b>產品別</b></span><br>
                                    <%--<label class="radio-inline">
                                        <input type="radio" name="inlineRadioProduct" id="inlineRadio1" value="" checked="" runat="server">
                                        全選
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="inlineRadioProduct" id="inlineRadio2" value="">
                                        居家生活
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="inlineRadioProduct" id="inlineRadio3" value="">
                                        母嬰
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="inlineRadioProduct" id="inlineRadio4" value="">
                                        辦公
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="inlineRadioProduct" id="inlineRadio5" value="">
                                        音樂運動休閒
                                    </label>--%>

                                    <asp:RadioButton ID="rbtn_area_all" class="radio-inline" GroupName="AreaMenu" Text="全選" Checked=" true" runat="server" />
                                    <asp:RadioButton ID="rbtn_area_living" class="radio-inline" GroupName="AreaMenu" Text="居家生活" runat="server" />
                                    <asp:RadioButton ID="rbtn_area_mombaby" class="radio-inline" GroupName="AreaMenu" Text="母嬰"  runat="server" />
                                    <asp:RadioButton ID="rbtn_area_office" class="radio-inline" GroupName="AreaMenu" Text="辦公" runat="server" />
                                    <asp:RadioButton ID="rbtn_area_leisure" class="radio-inline" GroupName="AreaMenu" Text="音樂運動休閒" runat="server" />
                                </div>
                                <div class="col-md-4 search-border search-right-section">
                                    <span class="search-modal-section-title"><b>使用地點</b></span>
                                    <br>
                                   <%-- <label class="radio-inline">
                                        <input type="radio" name="inlineRadioLocation" id="inlineRadio6" value="" checked="">
                                        全選
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="inlineRadioLocation" id="inlineRadio7" value="">
                                        客廳
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="inlineRadioLocation" id="inlineRadio8" value="">
                                        花園
                                    </label>--%>
                                    <asp:RadioButton ID="rbtn_location_all" class="radio-inline" GroupName="LocationMenu" Text="全選" Checked=" true" runat="server" />
                                    <asp:RadioButton ID="rbtn_location_livingroom" class="radio-inline" GroupName="LocationMenu" Text="客廳" runat="server" />
                                    <asp:RadioButton ID="rbtn_location_garden" class="radio-inline" GroupName="LocationMenu" Text=" 花園"  runat="server" />
                                </div>
                                <div class="col-md-1 search-right-section-space"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-1 search-left-section-space"></div>
                                <div class="col-md-6 search-border search-left-section">
                                    <span class="search-modal-section-title"><b>價格</b></span>
                                    <span id="select_price" runat="server">0 ~ 100000</span> 
                                    <asp:HiddenField id="hf_select_price" runat="server" />
                                    <b>新台幣</b><br>
                                    <b>0&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b>
                                    <input id="price_slider" type="text" class="span2" runat="server"  data-slider-min="0" data-slider-max="100000" data-slider-step="1000" data-slider-value="[0,100000]" />
                                    <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;100000</b>
                                </div> 
                                <div class="col-md-4 search-border search-right-section">
                                    <span class="search-modal-section-title"><b>資料來源</b></span>
                                    <br>
                                   <%-- <label class="radio-inline">
                                        <input type="radio" name="inlineRadioDataSource" id="inlineRadio9" value="" checked="">
                                        全選
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="inlineRadioDataSource" id="inlineRadio10" value="">
                                        美國
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="inlineRadioDataSource" id="inlineRadio11" value="">
                                        中國
                                    </label>--%>
                                    <asp:RadioButton ID="rbtn_lang_all" class="radio-inline" GroupName="LangMenu" Text="全選" Checked=" true" runat="server" />
                                    <asp:RadioButton ID="rbtn_lang_en" class="radio-inline" GroupName="LangMenu" Text="美國" runat="server" />
                                    <asp:RadioButton ID="rbtn_lang_zh" class="radio-inline" GroupName="LangMenu" Text="中國"  runat="server" />
                                </div>
                                <div class="col-md-1 search-right-section-space"></div>
                            </div>
                        </div>
                        <!-- end of container -->
                    </div>
                    <!-- end of modal-body -->
                     <div class="modal-footer">
						<img src="../images/emotion_step.png" class="sc-step-tip">
				    </div> 
                </div> 
            </div>
        </div>
               
                                    </asp:Panel>
        <!--end : 搜尋互動視窗 -->
         <!-- start : 加入收藏 -->
	<div class="modal fade" id="addCollectionhModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg sc-modal-dialog">
		    <div class="modal-content">
		    	<div class="modal-header">
					<span class="search-modal-title">加入收藏</span>
		      	</div>
		     	<div class="modal-body">
		     		<div class="container-fluid">
						<div class="row sc-modal-row">
							<div class="col-md-2 sc-modal-row-label">
							     <asp:RadioButton CssClass="Space"  ID="rbtn_AddNew" GroupName="CollectionAddWay" Text="新增收藏夾" Checked="true" runat="server" />
							</div>
							<div class="col-md-7">
	                            <asp:TextBox ID="txt_collection_name" class="form-control"  runat="server" meta:resourcekey="tbx_queryResource1" placeholder="輸入收藏名稱" onclick="NewCollectionClick();" ></asp:TextBox>
       				
                            </div>
						</div>
						<div class="row sc-modal-row">
							<div class="col-md-2 sc-modal-row-label">
                                 <asp:RadioButton  CssClass="Space"  ID="rbtn_AddOld"  GroupName="CollectionAddWay" Text="加入既有收藏夾" runat="server" />
							</div>
							<div class="col-md-7">
                                <asp:DropDownList ID="ddl_myCollection" runat="server" class="add-collection-select" onclick="MyCollectionClick();" > 
                                </asp:DropDownList>  
							</div>
							<div class="col-md-3 sc-modal-btn-area">
                                <asp:LinkButton ID="lbtn_goToCart_new" class="btn btn-sm sc-btn" runat="server" Text="收藏" OnClick="lbtn_goToCart_Click"  ></asp:LinkButton>
                                <%--<button type="button" class="btn btn-sm sc-btn" id="btn_collection_save">
				        			收藏
				        		</button>--%>
				        		<button type="button" class="btn btn-sm sc-btn" data-dismiss="modal">
				        			取消
				        		</button>
							</div>
						</div>
					</div>
		        	<!-- end of container -->
		        </div>
		        <!-- end of modal-body -->
		    </div>
		    
		</div>
	</div>
	<!-- end : 加入收藏 -->
        

        <div class="rev_slider_wrapper fullscreen-container">
            <div id="rev_slider" class="rev_slider fullscreenbanner" style="overflow: visible; height: 250px !important;">
                <ul>
                    <!-- SLIDE  -->
                    <li data-transition="random" data-slotamount="7" data-masterspeed="300">
                        <!-- MAIN IMAGE -->
                        <img src="../images/sliderbg.jpg" alt="slide1_background" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption sfb" data-x="450" data-y="0" data-speed="300" data-start="500" data-easing="easeOutBounce" data-endspeed="300" style="z-index: 2">
                            <img src="../images/slider1_1.png" alt="">
                        </div>

                    </li>
                    <!-- SLIDE  -->
                    <li data-transition="random" data-slotamount="7" data-masterspeed="300">
                        <!-- MAIN IMAGE -->
                        <img src="../images/sliderbg2.jpg" alt="black_linen_v2" data-bgfit="normal" data-bgposition="center top" data-bgrepeat="repeat">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption sfb" data-x="400" data-y="0" data-speed="300" data-start="500" data-easing="easeOutBounce" data-endspeed="300" style="z-index: 2">
                            <img src="../images/slider1_1.png" alt="">
                        </div>
                    </li>
                    <li data-transition="random" data-slotamount="7" data-masterspeed="300">
                        <!-- MAIN IMAGE -->
                        <img src="../images/sliderbg3.jpg" alt="black_linen_v2" data-bgfit="normal" data-bgposition="center top" data-bgrepeat="repeat">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption sfb" data-x="400" data-y="0" data-speed="300" data-start="500" data-easing="easeOutBounce" data-endspeed="300" style="z-index: 2">
                            <img src="../images/slider1_1.png" alt="">
                        </div>
                    </li>
            </div>
            </li>
        </ul>
        </div>
        <script type="text/javascript">
            var revapi1;
            jQuery(document).ready(function () {

                if (jQuery.fn.cssOriginal != undefined)
                    jQuery.fn.css = jQuery.fn.cssOriginal;

                if (jQuery('#rev_slider').revolution == undefined)
                    revslider_showDoubleJqueryError('#rev_slider');
                else
                    revapi1 = jQuery('#rev_slider').show().revolution(
                    {
                        delay: 9000,
                        startwidth: 1170,
                        startheight: 250,
                        hideThumbs: 200,

                        thumbWidth: 100,
                        thumbHeight: 50,
                        thumbAmount: 3,

                        navigationType: "bullet",
                        navigationArrows: "solo",
                        navigationStyle: "round",

                        touchenabled: "on",
                        onHoverStop: "on",

                        navigationHAlign: "center",
                        navigationVAlign: "bottom",
                        navigationHOffset: 0,
                        navigationVOffset: 20,

                        soloArrowLeftHalign: "left",
                        soloArrowLeftValign: "center",
                        soloArrowLeftHOffset: 20,
                        soloArrowLeftVOffset: 0,

                        soloArrowRightHalign: "right",
                        soloArrowRightValign: "center",
                        soloArrowRightHOffset: 20,
                        soloArrowRightVOffset: 0,

                        shadow: 0,
                        fullWidth: "on",
                        fullScreen: "off",

                        stopLoop: "off",
                        stopAfterLoops: -1,
                        stopAtSlide: -1,

                        shuffle: "off",

                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        startWithSlide: 0,
                        videoJsPath: "../js/revslider/js/videojs/",
                        fullScreenOffsetContainer: ""
                    });

            }); //ready
        </script>
        <header id="header">
            <div class="top-header">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-12 col-md-12 text-right top-header-right">

                            <div class="header-search pull-right visible-lg visible-md" id="header-search">
                                <form method="get" id="searchform" action="#">
                                    <div class="header-search-input-wrap">
                                        <input class="header-search-input" placeholder="Type to search..." type="text" value="" name="s" id="s" />
                                    </div>
                                    <input class="header-search-submit" type="submit" id="go" value=""><span class="header-icon-search"><i class="fa fa-search"></i></span>
                                </form>
                                <!-- // #searchform -->
                            </div>
                            <!-- // .header-search -->

                            <ul class="social list-unstyled pull-right">
                                <li class="blogger"><a data-toggle="tooltip" data-placement="bottom" title="Blogger Page" href="#"><span><b>B</b></span></a></li>
                                <li class="facebook"><a data-toggle="tooltip" data-placement="bottom" title="Facebook Page" href="#"><i class="fa fa-facebook"></i></a></li>
                                <li class="twitter"><a data-toggle="tooltip" data-placement="bottom" title="Follow us on Twitter" href="#"><i class="fa fa-twitter"></i></a></li>

                            </ul>
                            <!-- // .social -->

                        </div>
                        <!-- // .top-header-right -->

                    </div>
                    <!-- // .row -->
                </div>
                <!-- // .container -->
            </div>
            <!-- // .top-header -->

            <div class="main-header">
                <div class="inote">
                    <asp:Label ID="lbl_loginName" runat="server"></asp:Label>
                    您好
                    <asp:LinkButton ID="lbtn_logout" runat="server" Text="登出" OnClick="lbtn_logout_Click" meta:resourcekey="lbtn_logoutResource1"></asp:LinkButton>
                </div>
                <div class="container">

                    <div id="mainmenu2" class="site-menu visible-lg visible-md right-menu">

                        <div class="left-menu hide">
                            <div class="left-menu-info">
                                <form action="post">
                                    <input type="text" placeholder="Type to search" name="s">
                                    <button><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                            <!-- // .left-menu-info -->
                        </div>
                        <!-- // .left-menu.hide -->

                        <nav>
                            <ul class="menu sf-menu list-unstyled clearfix">
                                <li>
                                    <a href="../WebPage/index.aspx">首頁</a>
                                </li>

                                <li>
                                    <a href="page_about_unit.html">關於我們</a>
                                    <ul>
                                        <li><a href="page_alliance-rule.html">宗旨與服務</a></li>
                                        <li><a href="page_about_mission.html">願景與使命</a></li>
                                        <li><a href="page_about_research.html">研發重點</a></li>
                                        <li><a href="page_alliance-item.html">服務項目</a></li>
                                        <li><a href="page_alliance-join.html">加入會員</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="page_emotion-intro.html">感性設計</a>
                                    <ul>
                                        <li><a href="page_emotion-intro.html">感性設計介紹</a></li>
                                        <li><a href="page_emotion-example.html">應用實例</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="pape_energy-platform.html">技術能量</a>
                                    <ul>
                                        <li><a href="pape_energy-platform.html">感性設計系統平台</a></li>
                                        <li><a href="pape_energy-3D1.html">3D 快速成形</a></li>
                                        <li><a href="pape_energy-3D2.html">3D 光學掃描</a></li>
                                        <li><a href="pape_energy-action.html">無標籤動作攫起</a></li>
                                        <li><a href="pape_energy-pressure.html">接觸式壓力分布量測</a></li>
                                        <li><a href="pape_energy-eyes.html">眼球追蹤結合生理訊號系統</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="page_industry-lab.html">產業服務</a>
                                    <ul>
                                        <li>
                                            <a href="page_industry-lab.html">開放實驗室</a>
                                            <ul>
                                                <li><a href="page_industry-lab01.html">快速成型特色實驗室</a></li>
                                                <li><a href="page_industry-lab02.html">使用者行為</br>分析實驗室</a></li>
                                                <li><a href="page_industry-lab03.html">產品設計分析與</br>模擬實驗室</a></li>
                                                <li><a href="page_industry-lab04.html">創意文閣/</br>設計精品陳列室</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="page_industry-service.html">委託服務</a></li>
                                        <li><a href="page_industry-traning.html">教育訓練</a></li>
                                    </ul>
                                </li>

                                <li><a href="page_contact.html">聯絡我們</a></li>
                                <li><a href="page_news.html">最新消息</a></li>
                                <li><a href="UserLogin.aspx">會員登入</a></li>
                                <li class="current">
                                    <asp:LinkButton ID="lbtn_goToSearch" runat="server" OnClick="lbtn_goToSearch_Click" data-toggle="modal" data-target="#searchModal"  data-backdrop="static"
   data-keyboard="false">語意搜尋平台</asp:LinkButton>
                                    <%--<a href="../WebPage/SemanticSearch.aspx?"+Request.Querystring[]>語意搜尋平台</a>--%>
                                </li>
                            </ul>
                        </nav>
                        <!-- // nav -->
                    </div>
                    <!-- // .site-menu.right-menu -->
                    </br></br>

                <!-- Repsonsive Menu Trigger -->
                    <a class="pull-right responsive-menu visible-sm visible-xs" href="#panel-menu" id="responsive-menu"><i class="fa fa-bars"></i></a>
                    <!-- End Reposnvie Menu Trigger -->

                </div>
                <!-- // .container -->
            </div>
            <!-- // .main-header -->
        </header>



        <%-- 中間內容 (重要) --%>
        <div class="mao">
            <p><a href="index.aspx">首頁</a>&nbsp;>&nbsp;<a href="#"> 感性設計系統平台</a>&nbsp;>&nbsp;<a href="#"><span class="li">感性設計意象資料庫</span></a></p>
            <br />
            <div class="goo" style="padding-top:5px">
                <div class="ico">
                    <img src="../images/lo.png">
                </div>
                <p class="title" style="padding-top:4px">
                    &nbsp;&nbsp;感性設計意象資料庫</br><span class="xtit">&nbsp;&nbsp;</span>
                </p>
            </div>
            <br />
             <!-- 內容start -->
                <div class="twocol">
                    <div class="left" style="font-size:16px;">
                        <span class="font-myblue">搜尋條件 : </span><asp:Label ID="lbl_condition" runat="server" Text=""></asp:Label>
                        <br>
                        <br>
                        <span class="font-myblue">意象詞彙 : </span><asp:Label ID="lbl_imageTerms" runat="server" Text=""></asp:Label>
                        <%-- <label id="lbl_imUrlString" runat="server"></label>
                        <asp:HiddenField ID="hf_imUrlString" runat="server" />--%>
                    </div>
                </div>
  
               <br />
                  <div id="div_TitleBar" runat="server" class="margin10TB">
                                <asp:Label class="DownBarTitle"  runat="server" >【搜尋結果】</asp:Label>
                                 <asp:Label class="DownGrayBar"  runat="server">選擇商品後，於右側點選 "加入收藏"</asp:Label>
                  </div>

            <div class="row">
               <br />
                <!-- twocol --> 
                <asp:UpdatePanel ID="up_products" runat="server" >
                    <ContentTemplate>
                        <!-- 瀑布流 -->
                        <div class="grid">
                            <asp:Repeater ID="rpt_productContents" runat="server">
                                <ItemTemplate> 
                                    <div class="grid-item">
                                        <checkbox id="cb_productCheck" title=" <%# DataBinder.Eval(Container.DataItem, "title")%>" class="shoppingcheck" onclick="productCount('<%# DataBinder.Eval(Container.DataItem, "imUrl")%>');return true;"></checkbox>
                                        <div class="proitemblock">
                                            <div >
                                                <asp:LinkButton ID="lbtn_productTitle" runat="server" CssClass="font-size4"> <%# DataBinder.Eval(Container.DataItem, "title_fix")%></asp:LinkButton>
                                            </div> 
                                            <br>
                                                <div style="text-align:right;">
                                                     <a href='<%# DataBinder.Eval(Container.DataItem, "link")%>' class="btn_original_link" target="_blank">原始連結</a>
                                                </div> 
                                            <br>
                                            <img style="max-height:300px;margin:0 auto;" src="<%# DataBinder.Eval(Container.DataItem, "imUrl")%>" class="img-responsive" />
                                            <%--Price:<asp:Label ID="lbl_price" runat="server" Text=""><%# DataBinder.Eval(Container.DataItem, "price")%></asp:Label>--%>
                                            <br>
                                              <span class="span_lang"><%# DataBinder.Eval(Container.DataItem, "lang_fix")%></span>
                                            <br> 
                                            <asp:Label ID="lbl_text" runat="server" Text=""><%# DataBinder.Eval(Container.DataItem, "text")%></asp:Label>
                                        </div>
          </div>


                                </ItemTemplate>
                            </asp:Repeater>
                        </div>

                    </ContentTemplate> 
                </asp:UpdatePanel>

                <!-- 內容end -->
            </div>
            </br>
        </div>

        <footer id="footer" <%--style="position: absolute; width: 100%;"--%>>
            <div id="footer-1" class="widget-area">
                <div class="container">
                    <div class="row">

                        <div class="widget quick-contact col-lg-9 col-md-9 bottom-30-sm bottom-30-xs">
                            <ul class="footernav">
                                <li>
                                    <a href="page_about_unit.html"><b style="font-size: 15px;">關於我們</b></a> </br>
                                <a href="page_alliance-rule.html">宗旨與服務</a></br>
                                <a href="page_about_mission.html">願景與使命</a></br>
                                <a href="page_about_research.html">研發重點</a></br>
                                <a href="page_alliance-item.html">服務項目</a></br>
                                <a href="page_alliance-join.html">加入會員</a>
                                </li>
                                <li>
                                    <a href="#"><b style="font-size: 15px;">感性設計</b></a> </br>
                                <a href="page_emotion-intro.html">感性設計介紹</a></br>
                                <a href="page_emotion-example.html">應用實例</a>
                                </li>
                                <li>
                                    <a href="#"><b style="font-size: 15px;">技術能量</b></a> </br>
                                <a href="pape_energy-platform.html">感性設計系統平台</a></br>
                                <a href="pape_energy-3D1.html">3D 快速成形</a></br>
                                <a href="pape_energy-3D2.html">3D 光學掃描</a></br>
                                <a href="pape_energy-action.html">無標籤動作攫起</a></br>
                                <a href="pape_energy-pressure.html">接觸式壓力分布量測</a></br>
                                <a href="pape_energy-eyes.html">眼球追蹤結合生理訊號系統</a>
                                </li>
                                <li>
                                    <a href="#"><b style="font-size: 15px;">產業服務</b></a></br>
                                <a href="page_industry-lab.html">開放實驗室</a></br>
                                <a href="page_industry-service.html">委託服務</a></br>
                                <a href="page_industry-traning.html">教育訓練</a>
                                </li>
                                <li>
                                    <a href="page_contact.html"><b style="font-size: 15px;">聯絡我們</b></a>
                                </li>
                                <li>
                                    <a href="page_news.html"><b style="font-size: 15px;">最新消息</b></a>
                                </li>
                            </ul>
                        </div>
                        <!-- // .widget -->

                        <div class="widget col-lg-3 col-md-3 bottom-30-sm bottom-30-xs">
                            <img src="../images/footer-logo.gif" alt="">
                            <p class="top-10" style="color: #BBB;">業務窗口：郝任珍(分機:97878)</br>網站製作：資訊中心｜問題反應｜系統使用說明書｜網站地圖｜</p>
                        </div>
                        <!-- // .widget -->

                    </div>
                    <!-- // .row -->
                </div>
                <!-- // .container -->
            </div>
            <!-- // .widget-area -->

            <div class="credit">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <span class="fed">版權所有 © 2014 工業技術研究院</span>
                        </div>
                    </div>
                    <!-- // .row -->
                </div>
                <!-- // .container -->
            </div>
            <!-- // .credit -->
        </footer>

        <%-- 左側搜尋與購物車區塊 --%>
        <div id="ForsearchBlock"  >

            <div id="Forsearchbtn" style="margin:3px">
                <div class="shoppingcartnumber" style="position:absolute;margin:1px">
                        <asp:Label ID="lbl_productCount" runat="server" Visible="true" Text="0"></asp:Label>
                    </div>
                <asp:LinkButton ID="lbtn_add_favorite" runat="server" Text="加入收藏" meta:resourcekey="lbtn_searchResource1" data-toggle="modal" data-target="#addCollectionhModal" data-backdrop="static" data-keyboard="false" disabled> <img width="74px" height="39px" src="../images/btn-add-collection.png"></asp:LinkButton>
            </div>
              <div id="Forsearchbtn" style="margin:3px" ">
                <a title="條件搜尋" data-toggle="modal" data-target="#searchModal" > <img width="74px" height="39px" src="../images/btn-search.png"></a>
              </div>
              <div id="Forsearchbtn" style="margin:3px">
                <a title="我的收藏" onclick="goToCart();return true;"> <img width="74px" height="39px" src="../images/btn-my-collection.png"></a>
              </div>
            <!-- Forsearchbtn -->
            <div id="Forshoppingcart" style="display:none">
                <div class="text-center">已加入</div>
                <div class="margin10T">
                    <asp:LinkButton ID="lbtn_goToCart" runat="server" meta:resourcekey="lbtn_goToCartResource1" OnClick="lbtn_goToCart_Click"> <img src="../images/icon-shoppingcartnow.png"></asp:LinkButton>
                  
                </div>
                <div class="text-center margin15T">我的最愛籃</div>
                <asp:Repeater ID="rpt_cartCount" runat="server">
                    <ItemTemplate>
                        <div class="shoppingcartinline">
                            <a id="cartPic" href="#" class="itemhint" title="<%# DataBinder.Eval(Container.DataItem, "c_name")%>" onclick="goToCart();return true;">
                                <img src="../images/btn-othershoppingcart.png">
                            </a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
            <!-- Forshoppingcart -->
        </div>
       





        <!-- Core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../js/bootstrap.js"></script>
        <script src="../js/easing.js"></script>
        <script src="../js/superfish.js"></script>
        <script src="../js/fitvids.js"></script>
        <script src="../js/flexslider.js"></script>
        <script src="../js/mediaelement.js"></script>
        <script src="../js/isotope.js"></script>
        <script src="../js/easypiechart.js"></script>
        <script src="../js/caroufredsel.js"></script>
        <script src="../js/jpanelmenu.js"></script>
        <script src="../js/magnific.js"></script>
        <script src="../js/twitter/tweet.js"></script>
        <script src="../js/functions.js"></script>
        <!-- Revolutions slider -->



        <script src="../rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
        <script src="../rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("a[rel='ja-colorbox']").colorbox();
            });
            function changeImage(object, ahref) {
                $("div.product-img-box li").removeClass("active");
                $(object).parent("li").addClass("active");
                $(this).addClass("active");
                $("#pimage").attr("src", object.href);
                $("a[rel='ja-colorbox']").attr("href", ahref);
                return false;
            }
        </script>



        <script type="text/javascript" src="../js/jquery-migrate-1.2.1.js"></script>
        <script type="text/javascript" src="../js/jquery.colorbox.js"></script>
        <script type="text/javascript" src="../js/function.js"></script>
        <script type="text/javascript" src="../js/jquery.easytabs.min.js"></script>
        <!-- easytabs tab -->
        <script type="text/javascript" src="../js/jquery.powertip.min.js"></script>
        <!-- powertip:tooltips -->
        <script type="text/javascript">
            $(".colorboxGen").colorbox({
                inline: true, width: "50%", maxWidth: "900", MaxHeight: "80%", opacity: 0.5, onOpen: function () {
                    $('#SearchPage').append($('#cboxOverlay'));
                    $('#SearchPage').append($('#colorbox'));
                }
            });

            $(".closecolorbox").click(function () {
                $.colorbox.close()
            })
            $('#tab-container').easytabs({

            });
            //點選切換樣式
            $(".shoppingcheck").click(function () {
                $(this).toggleClass("shoppingcheckup"); 
            })
        </script> 

    </form>
</body>
</html>
