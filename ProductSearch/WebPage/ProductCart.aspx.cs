﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Web.UI.WebControls;
using System.Reflection;
using System.Web.UI;

public partial class WebPage_ProductCart : common
{
    public DataView dv_cartContent;
    public DataTable dt_cartUrlSet;
    public DataTable dt_cartUrlSet_minus;
    public DataView dv_productScore;
    public DataTable dt_productScore;
    public string str_AllPickImgUrl = "";
    public string str_AllPickImgId = "";

    ProductCart_DAL productCart = new ProductCart_DAL();
    Cart_Model cart = new Cart_Model();
    Product_Model product = new Product_Model();

    publicFunction pubFun = new publicFunction();
    /// <summary>
    /// 登入者帳號
    /// </summary>
    public string loginAccount
    {
        set { ViewState["loginAccount"] = value; }
        get
        {
            if (ViewState["loginAccount"] == null)
                return string.Empty;
            return ViewState["loginAccount"].ToString();
        }
    }


    /// <summary>
    /// 儲存最終產品與分數
    /// </summary>
    public Dictionary<string, string[,]> vs_finalImgUrlString
    {
        set { ViewState["finalImgUrlString"] = value; }
        get
        {
            if (ViewState["finalImgUrlString"] == null)
                return null;
            return (Dictionary<String, string[,]>)ViewState["finalImgUrlString"];
        }
    }



    protected void Page_Load(object sender, EventArgs e)
    {

        lbl_loginName.Text = Convert.ToString(Session["user_id"]);
        cart.user_id = Convert.ToString(Session["user_id"]);
        if (!IsPostBack)
        {
            if (Convert.ToString(Session["user_id"]) == "")
            {
                    Response.Write("<Script language='JavaScript'>alert('請登入！');</Script>");
                    Response.Write("<script>window.parent.location='" + "../WebPage/UserLogin.aspx" + "'</script>");
            }
            else { 
            //讀取購物車資料
            getCartContent(cart);

            //最終產品圖片與分數清空
            vs_finalImgUrlString = new Dictionary<string, string[,]>();

            }
        }

        //修改購物車名稱_2016101_howard
        if ((Request.QueryString["new_c_name"]) != null)
        {
            cart.c_id = Convert.ToInt32(Request.QueryString["updated_c_id"]);
            cart.c_name = (Request.QueryString["new_c_name"]);

            if (productCart.isCartNameExist(cart.c_name, cart.user_id))
            {
                Response.Write("<script>alert('修改失敗，此收藏夾已存在')</script>");
            }
            else if (cart.c_name == "")
            {
                Response.Write("<script>alert('修改失敗，收藏夾名稱不能為空')</script>");
            }
            else
                updateCartName(cart);
        }

        //刪除購物車_2016101_howard
        if (Convert.ToInt32(Request.QueryString["deleted_c_id"]) > 0)
        {
            cart.c_id = Convert.ToInt32(Request.QueryString["deleted_c_id"]);
            deleteCart(cart);
        }

        //清除 querystring 避免重複修改或刪除_20161019_howard
        PropertyInfo isreadonly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
        isreadonly.SetValue(this.Request.QueryString, false, null);
        this.Request.QueryString.Remove("updated_c_id");
        this.Request.QueryString.Remove("new_c_name");
        this.Request.QueryString.Remove("deleted_c_id");
    }

    /// <summary>
    /// 取得個人所有購物車內容 (包含將資料表列轉成行的功能)_20161018_howard
    /// </summary>
    /// <param name="cart"></param>
    public void getCartContent(Cart_Model cart)
    {
        dv_cartContent = productCart.getCartContent(cart);

        DataTable dt_cid = new DataTable();
        dt_cid = dv_cartContent.ToTable(true, "c_id");

        DataTable dt_cartContent = new DataTable();
        dt_cartContent = dv_cartContent.ToTable();

        DataTable dt_finalCartContent = new DataTable();
        dt_finalCartContent.Columns.Add("c_id", typeof(int));
        dt_finalCartContent.Columns.Add("c_name", typeof(string));
        dt_finalCartContent.Columns.Add("mainPicUrl", typeof(string));
        dt_finalCartContent.Columns.Add("subPicUrl1", typeof(string));
        dt_finalCartContent.Columns.Add("subPicUrl2", typeof(string));
        dt_finalCartContent.Columns.Add("subPicUrl3", typeof(string));

        DataRow dr_tempUrl = dt_finalCartContent.NewRow();

        int picCount = 1; 
        foreach (DataRow dr in dt_cid.Rows)
        {
            dr_tempUrl = dt_finalCartContent.NewRow();
            dr_tempUrl["c_id"] = dr["c_id"];
            picCount = 1;
            foreach (DataRow dr2 in dt_cartContent.Rows)
            {
                if ((dr2["c_id"].Equals(dr["c_id"])) && (picCount < 5))//若每台車取得照片小於5張，就繼續取
                {
                    switch (picCount)
                    {
                        case 1:
                            dr_tempUrl["c_name"] = dr2["c_name"];
                            dr_tempUrl["mainPicUrl"] = DoRetrunSpecialChar(Convert.ToString(dr2["p_imgUrl"]));
                            break;
                        case 2:
                            dr_tempUrl["subPicUrl1"] = DoRetrunSpecialChar(Convert.ToString(dr2["p_imgUrl"]));
                            break;
                        case 3:
                            dr_tempUrl["subPicUrl2"] = DoRetrunSpecialChar(Convert.ToString(dr2["p_imgUrl"]));
                            break;
                        case 4:
                            dr_tempUrl["subPicUrl3"] = DoRetrunSpecialChar(Convert.ToString(dr2["p_imgUrl"]));
                            break;
                    }
                    picCount += 1;
                }
                else if (picCount >= 5)
                {
                    break;
                }
            }
            dt_finalCartContent.Rows.Add(dr_tempUrl);
        }
        rpt_cartContent.DataSource = dt_finalCartContent;
        rpt_cartContent.DataBind();

        lbl_cartCount.Text = dt_finalCartContent.Rows.Count.ToString();
        lbl_PicCount.Text = dt_cartContent.Rows.Count.ToString();
    }

    /// <summary>
    /// 修改購物車名稱_20161018_howard
    /// </summary>
    /// <param name="cart"></param>
    public void updateCartName(Cart_Model cart)
    {
        productCart.updateCartName(cart);
        Response.Write("<Script language='JavaScript'>alert('名稱修改成功！');</Script>");
        getCartContent(cart);
    }

    /// <summary>
    /// 刪除購物車_20161018_howard
    /// </summary>
    /// <param name="c_id"></param>
    public void deleteCart(Cart_Model cart)
    {
        if (productCart.deleteCart(cart))
        {
            Response.Write("<Script language='JavaScript'>alert('收藏夾刪除成功！');</Script>");
            getCartContent(cart);
        }
    }

    /// <summary>
    /// 點選購物車，顯示購物車內容_V2_20161018_howard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_confirmCheck_Click(object sender, EventArgs e)
    {
        if (vs_finalImgUrlString.Count == 0)
        {
            dt_cartUrlSet = new DataTable();

            string[] c_idString = Regex.Split(HttpContext.Current.Request.Cookies["c_idString"].Value, "_id_", RegexOptions.IgnoreCase);

 	    Dictionary<Object, Object> temp = new Dictionary<Object, Object>();
                
            for (int i = 1; i <= c_idString.Length - 1; i++)
            {
                cart.c_id = Convert.ToInt32(c_idString[i]);	
		        //dt_cartUrlSet.Merge(productCart.selectCart(cart).ToTable());
		        if(i == 1)
		        {
		            dt_cartUrlSet.Merge(productCart.selectCart(cart).ToTable());		
 		            foreach (DataRow row in dt_cartUrlSet.Rows){
			        temp.Add(row[2],row[3]);
		            }
		        }else{
			        DataTable dt = productCart.selectCart(cart).ToTable();
			        foreach (DataRow row in dt.Rows)
               	 	        {
			          if(!temp.ContainsKey(row[2]) && !temp.ContainsValue(row[3])){
			  	        temp.Add(row[2],row[3]);
		  		        dt_cartUrlSet.Rows.Add(row.ItemArray);
		 	          }
			        }
		        }
		
            }
            str_AllPickImgUrl = "finalImgUrlString=";
            str_AllPickImgId = "finalImgIdString=";

           
		
            //綁定產品圖片
            foreach (DataRow row in dt_cartUrlSet.Rows)
            {
                row[3] = DoRetrunSpecialChar(Convert.ToString(row[3]));
		
	        str_AllPickImgId += "_id_" + row[2];
                str_AllPickImgUrl += "_url_" + row[3];
		
            }

            this.hf_AllPick_ImgID.Value = str_AllPickImgId;
            this.hf_AllPick_ImgUrl.Value = str_AllPickImgUrl;

            rpt_cartUrls.DataSource = dt_cartUrlSet;
            rpt_cartUrls.DataBind();
        }
        else
        {
            //重新綁定購物車內產品 (去除已選擇的)
            dt_cartUrlSet = new DataTable();

            string[] c_idString = Regex.Split(HttpContext.Current.Request.Cookies["c_idString"].Value, "_id_", RegexOptions.IgnoreCase);
            for (int i = 1; i <= c_idString.Length - 1; i++)
            {
                cart.c_id = Convert.ToInt32(c_idString[i]);
                dt_cartUrlSet.Merge(productCart.selectCart(cart).ToTable());

            }

            dt_cartUrlSet_minus = new DataTable();
            DataColumn column = new DataColumn("p_imgUrl");
            column.DataType = System.Type.GetType("System.String");
            DataColumn column2 = new DataColumn("p_id");
            column.DataType = Type.GetType("System.String");
            DataColumn column3 = new DataColumn("p_name");
            column.DataType = Type.GetType("System.String");
            DataColumn column4 = new DataColumn("p_oriUrl");
            column.DataType = Type.GetType("System.String");
            DataColumn column5 = new DataColumn("p_review");
            column.DataType = Type.GetType("System.String");
            dt_cartUrlSet_minus.Columns.Add(column);
            dt_cartUrlSet_minus.Columns.Add(column2);
            dt_cartUrlSet_minus.Columns.Add(column3);
            dt_cartUrlSet_minus.Columns.Add(column4);
            dt_cartUrlSet_minus.Columns.Add(column5);
            foreach (DataRow row in dt_cartUrlSet.Rows)
            {
                if (vs_finalImgUrlString.ContainsKey(DoRetrunSpecialChar(Convert.ToString(row["p_imgUrl"]))) == false)
                {
                    DataRow workRow = dt_cartUrlSet_minus.NewRow();
                    workRow["p_imgUrl"] = DoRetrunSpecialChar(Convert.ToString(row["p_imgUrl"]));
                    workRow["p_id"] = DoRetrunSpecialChar(Convert.ToString(row["p_id"]));
                    workRow["p_name"] = DoRetrunSpecialChar(Convert.ToString(row["p_name"]));
                    workRow["p_oriUrl"] = DoRetrunSpecialChar(Convert.ToString(row["p_oriUrl"]));
                    workRow["p_review"] = DoRetrunSpecialChar(Convert.ToString(row["p_review"]));
                    dt_cartUrlSet_minus.Rows.Add(workRow);
                }
            }


            //重新綁定產品圖片
            rpt_cartUrls.DataSource = dt_cartUrlSet_minus;
            rpt_cartUrls.DataBind();
        }
         

    }

    /// <summary>
    /// 顯示產品意象圖內容_20161019_howard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_confirmCheck2_Click(object sender, EventArgs e)
    {
        string[] finalImgUrlString = Regex.Split(HttpContext.Current.Request.Cookies["finalImgUrlString"].Value, "_url_", RegexOptions.IgnoreCase);
        string[] finalImgIdString = Regex.Split(HttpContext.Current.Request.Cookies["finalImgIdString"].Value, "_id_", RegexOptions.IgnoreCase);

        if (finalImgUrlString.Length == 1 || finalImgIdString.Length == 1) //確認有無勾選物件
        { 
            //沒有勾選任何物件~把值存回hidden field~避免postBack清掉cookie
            //重新綁定購物車內產品
            dt_cartUrlSet = new DataTable();

            string[] c_idString1 = Regex.Split(HttpContext.Current.Request.Cookies["c_idString"].Value, "_id_", RegexOptions.IgnoreCase);
            for (int i = 1; i <= c_idString1.Length - 1; i++)
            {
                cart.c_id = Convert.ToInt32(c_idString1[i]);
                dt_cartUrlSet.Merge(productCart.selectCart(cart).ToTable());
            }

            //===重新把Allpick值塞回HiddenField===
            str_AllPickImgUrl = "finalImgUrlString=";
            str_AllPickImgId = "finalImgIdString=";
    
            //綁定產品圖片
            foreach (DataRow row in dt_cartUrlSet.Rows)
            {
                row[3] = DoRetrunSpecialChar(Convert.ToString(row[3]));

                str_AllPickImgId += "_id_" + row[2];
                str_AllPickImgUrl += "_url_" + row[3];
            }

            this.hf_AllPick_ImgID.Value = str_AllPickImgId;
            this.hf_AllPick_ImgUrl.Value = str_AllPickImgUrl;

            //===========================以上=====================

            return;
        }

        for (int i = 1; i <= finalImgUrlString.Length - 1; i++)
        {
            finalImgUrlString[i] = finalImgUrlString[i].Replace("http://ec4.images嚗", "http://ec4.images－a");
            finalImgUrlString[i] = finalImgUrlString[i].Replace("http://ec8.images嚗", "http://ec8.images－a");
            if (vs_finalImgUrlString.ContainsKey(finalImgUrlString[i]) == false)
            {
                //用 p_id 找到產品分數，並且 add 進 vs_finalImgUrlString
                product.p_id = Convert.ToInt32(finalImgIdString[i]);
                dv_productScore = productCart.selectProductScore(product);
                int rowCount = dv_productScore.ToTable().Rows.Count;
                int columnCount = dv_productScore.ToTable().Columns.Count;

                string[,] arr = new string[rowCount, columnCount];

                for (int j = 0; j < rowCount; j++)
                {
                    for (int k = 0; k < columnCount; k++)
                    {
                        arr[j, k] = Convert.ToString(dv_productScore.ToTable().Rows[j][k]);
                    }
                }
                vs_finalImgUrlString.Add(finalImgUrlString[i], arr); 

            }
        }


        //
        //產生詞彙班長 checkboxlist_20161024_howard
        if (dv_productScore == null)
            return;
        DataView dv_productScoreZero = dv_productScore;
        dv_productScoreZero.RowFilter = @"ps_score<>'0'";
        DataTable dt_termLeader = dv_productScoreZero.ToTable(true, "ps_termLeader");
        
        cbxl_termLeader.Items.Clear();
        cbxl_termLeader.Visible = true;
        //cbxl_termLeader.AutoPostBack = true;
        //cbxl_termLeader.CellPadding = 20;
        //cbxl_termLeader.CellSpacing = 20;
        //cbxl_termLeader.RepeatColumns = 10;
        //cbxl_termLeader.RepeatLayout = RepeatLayout.Flow;
        //cbxl_termLeader.TextAlign = TextAlign.Right;


        foreach (DataRow row in dt_termLeader.Rows)
        {
            //cbxl_termLeader.Items.Add(Convert.ToString(row["ps_termLeader"]));
            cbxl_termLeader.Items.Add(Convert.ToString(row["ps_termLeader"]));
            //cbxl_termLeader.Text = Convert.ToString(row["ps_termLeader"]);
            //cbxl_termLeader.ID= Convert.ToString(row["ps_termLeader"]);
           
        }

        foreach (ListItem li in cbxl_termLeader.Items)//賦予每個checkbox按下都會去執行不超過2個的方法
        {
            li.Attributes.Add("onclick", "termLeaderCantOverTwo()");
        }

        //綁定產品圖片
        rpt_fianlCartUrls.DataSource = vs_finalImgUrlString;
        rpt_fianlCartUrls.DataBind();


        //重新綁定購物車內產品 (去除已選擇的)
        dt_cartUrlSet = new DataTable();

        string[] c_idString = Regex.Split(HttpContext.Current.Request.Cookies["c_idString"].Value, "_id_", RegexOptions.IgnoreCase);
        for (int i = 1; i <= c_idString.Length - 1; i++)
        {
            cart.c_id = Convert.ToInt32(c_idString[i]);
            dt_cartUrlSet.Merge(productCart.selectCart(cart).ToTable());
        }

        //===重新把Allpick值塞回HiddenField~不然經過這次postback值會消失~全選的功能也會失效===
        str_AllPickImgUrl = "finalImgUrlString=";
        str_AllPickImgId = "finalImgIdString=";

        //綁定產品圖片
        foreach (DataRow row in dt_cartUrlSet.Rows)
        {
            row[3] = DoRetrunSpecialChar(Convert.ToString(row[3]));
            str_AllPickImgId += "_id_" + row[2];
            str_AllPickImgUrl += "_url_" + row[3];
        }

        this.hf_AllPick_ImgID.Value = str_AllPickImgId;
        this.hf_AllPick_ImgUrl.Value = str_AllPickImgUrl;

        //===========================以上=====================


        dt_cartUrlSet_minus = new DataTable();
        DataColumn column = new DataColumn("p_imgUrl");
        column.DataType = System.Type.GetType("System.String");
        DataColumn column2 = new DataColumn("p_id");
        column.DataType = Type.GetType("System.String");
        DataColumn column3 = new DataColumn("p_name");
        column.DataType = Type.GetType("System.String");
        DataColumn column4 = new DataColumn("p_oriUrl");
        column.DataType = Type.GetType("System.String");
        DataColumn column5 = new DataColumn("p_review");
        column.DataType = Type.GetType("System.String");
        dt_cartUrlSet_minus.Columns.Add(column);
        dt_cartUrlSet_minus.Columns.Add(column2);
        dt_cartUrlSet_minus.Columns.Add(column3);
        dt_cartUrlSet_minus.Columns.Add(column4);
        dt_cartUrlSet_minus.Columns.Add(column5);
        foreach (DataRow row in dt_cartUrlSet.Rows)
        {
            if (vs_finalImgUrlString.ContainsKey(DoRetrunSpecialChar(Convert.ToString(row["p_imgUrl"]))) == false)
            {
                DataRow workRow = dt_cartUrlSet_minus.NewRow();
                workRow["p_imgUrl"] = DoRetrunSpecialChar(Convert.ToString(row["p_imgUrl"]));
                workRow["p_id"] = DoRetrunSpecialChar(Convert.ToString(row["p_id"]));
                workRow["p_name"] = DoRetrunSpecialChar(Convert.ToString(row["p_name"]));
                workRow["p_oriUrl"] = DoRetrunSpecialChar(Convert.ToString(row["p_oriUrl"]));
                workRow["p_review"] = DoRetrunSpecialChar(Convert.ToString(row["p_review"]));
                dt_cartUrlSet_minus.Rows.Add(workRow);
            }
        }


        //重新綁定產品圖片
        rpt_cartUrls.DataSource = dt_cartUrlSet_minus;
        rpt_cartUrls.DataBind();


    }

    protected void Cbxl_termLeader_SelectedIndexChanged(object sender, EventArgs e)
    {

        throw new NotImplementedException();
    }

    /// <summary>
    /// 清空所選擇的產品_20161020_howard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_resetPic_Click(object sender, EventArgs e)
    {
        vs_finalImgUrlString = new Dictionary<string, string[,]>();
        rpt_cartUrls.DataSource = null;
        rpt_cartUrls.DataBind();

        rpt_fianlCartUrls.DataSource = null;
        rpt_fianlCartUrls.DataBind();

        cbxl_termLeader.Items.Clear();
        //清除 querystring 避免重複修改或刪除_20161019_howard
        PropertyInfo isreadonly = typeof(System.Collections.Specialized.NameValueCollection).GetProperty("IsReadOnly", BindingFlags.Instance | BindingFlags.NonPublic);
        isreadonly.SetValue(this.Request.QueryString, false, null);
        this.Request.QueryString.Remove("updated_c_id");
        this.Request.QueryString.Remove("new_c_name");
        this.Request.QueryString.Remove("deleted_c_id"); 

    }

    /// <summary>
    /// 回上一步~會清空意象分析內容
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_BackStep_Click(object sender, EventArgs e)
    {
        dt_cartUrlSet = new DataTable();

        string[] c_idString = Regex.Split(HttpContext.Current.Request.Cookies["c_idString"].Value, "_id_", RegexOptions.IgnoreCase);

        for (int i = 1; i <= c_idString.Length - 1; i++)
        {
            cart.c_id = Convert.ToInt32(c_idString[i]);
            dt_cartUrlSet.Merge(productCart.selectCart(cart).ToTable());

        }
        str_AllPickImgUrl = "finalImgUrlString=";
        str_AllPickImgId = "finalImgIdString=";
         
        //綁定產品圖片
        foreach (DataRow row in dt_cartUrlSet.Rows)
        {
            row[3] = DoRetrunSpecialChar(Convert.ToString(row[3]));
            str_AllPickImgId += "_id_" + row[2];
            str_AllPickImgUrl += "_url_" + row[3];
        }

        this.hf_AllPick_ImgID.Value = str_AllPickImgId;
        this.hf_AllPick_ImgUrl.Value = str_AllPickImgUrl;

        //復原
        rpt_cartUrls.DataSource = dt_cartUrlSet;
        rpt_cartUrls.DataBind();

        //清除
        rpt_fianlCartUrls.DataSource = null;
        rpt_fianlCartUrls.DataBind();

        vs_finalImgUrlString = new Dictionary<string, string[,]>();
        cbxl_termLeader.Items.Clear();

    }


    /// <summary>
    /// 去搜尋_20161103_howard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_goToSearch_Click(object sender, EventArgs e)
    {
        Response.Write("<script>window.parent.location='" + "../WebPage/SemanticSearch.aspx?loginName=" + lbl_loginName.Text + "'</script>");
    }
    /// <summary>
    /// 登出_20161103_howard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_logout_Click(object sender, EventArgs e)
    {

        Session["user_id"] = "";
        Session["user_st"] = "";
        Response.Write("<script>window.parent.location='" + "../WebPage/index.aspx" + "'</script>");
        lbtn_logout.Visible = false;

    }
}