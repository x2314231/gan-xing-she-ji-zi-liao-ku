﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class WebPage_ValidateCode : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["verify"] != null)
            ValidateCode1(Session["verify"].ToString());
    }

    private void ValidateCode1(string VNum)
    {
        Bitmap Img = null;
        Graphics g = null;
        MemoryStream ms = null;
        int gheight = VNum.Length * 9;
        Img = new Bitmap(63, 25);
        g = Graphics.FromImage(Img);
        //背景颜色
        g.Clear(Color.WhiteSmoke);
        //文字字体
        Font f = new Font("Tahoma", 12);
        //文字颜色
        SolidBrush s = new SolidBrush(Color.Red);
        g.DrawString(VNum, f, s, 3, 3);
        ms = new MemoryStream();
        Img.Save(ms, ImageFormat.Jpeg);
        Response.ClearContent();
        Response.ContentType = "image/Jpeg";
        Response.BinaryWrite(ms.ToArray());
        g.Dispose();
        Img.Dispose();
        Response.End();
    }

    private string RndNum(int VcodeNum)
    {
        string MaxNum = "";
        string MinNum = "";
        for (int i = 0; i < 5; i++)//這裡的5是驗證碼的位數
        {
            MaxNum = MaxNum + "5";
        }
        MinNum = MaxNum.Remove(0, 1);
        Random rd = new Random();
        string VNum = Convert.ToString(rd.Next(Convert.ToInt32(MinNum), Convert.ToInt32(MaxNum)));
        return VNum;

        //Validate Code
        //string[] Code ={ "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
        //          "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
        //          "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        //string strRd = string.Empty;
        //for (int i = 0; i < 5; i++)       // 亂數產生驗證文字
        //{
        //  strRd += Code[rdNext(35)];
        // }
    }
}