﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Registration;
using System.IO;

public partial class WebPage_index : System.Web.UI.Page
{
    publicFunction pubFun = new publicFunction();
    private string ImgUpLoadPath = "";//Registration.Common.getUpLoadPath + "\\NewsImg\\";

    Registration.News_DB NewsDB = new Registration.News_DB();
    protected void Page_Load(object sender, EventArgs e)
    {

        lbl_loginName.Text = Convert.ToString(Session["user_id"]);
        if(lbl_loginName.Text=="")
        {
            lbtn_logout.Visible = false;
        }
        else
        {
            lbtn_logout.Visible = true;
        }


        if (!IsPostBack)
            this.InitPage();
    }
    /// <summary>
    /// 初始化頁面
    /// </summary>
    private void InitPage()
    {
        this.BindNewsList();
    }

    #region 最新消息清單

    /// <summary>
    /// 繫結最新消息清單
    /// </summary>
    private void BindNewsList()
    {
        NewsDB._news_class = "";
        NewsDB._NowDate = DateTime.Now.ToString("yyyyMMdd");
        this.rptNewsList.DataSource = NewsDB.SelectNews().DefaultView;
        this.rptNewsList.DataBind();
    }

    protected void rptNewsList_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item ||
            e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView drv = e.Item.DataItem as DataRowView;

            // 標題
            Literal ltrTitle = e.Item.FindControl("ltrTitle") as Literal;
            ltrTitle.Text = drv["news_title"].ToString();


            // 圖片
            string filename = drv["attachment_changefilename"].ToString().Trim(),
                   path = string.Format("{0}{1}", this.ImgUpLoadPath, filename);
            if (File.Exists(path))
                path = string.Format("~/DOWNLOAD.aspx?_FileName={0}&_oldName={0}&_FileType=NewsImg", filename);
            else
                path = "~/images/default220x220.gif";


            Image img = e.Item.FindControl("img") as Image;
            img.ImageUrl = path;
            img.AlternateText = ltrTitle.Text.Trim();

            HyperLink hypNewsImage = e.Item.FindControl("hypNewsImage") as HyperLink;
            hypNewsImage.NavigateUrl = string.Format("~/NewsView.aspx?id={0}", Server.UrlEncode(Registration.Common.Md5Encrypt(drv["news_id"].ToString())));


            // 敘述
            string content = Registration.Common.NoHTML(drv["news_content"].ToString());
            if (content.Length > 70)
                content = string.Format("{0}...", content.Substring(0, 30));

            HyperLink hypNewsContent = e.Item.FindControl("hypNewsContent") as HyperLink;
            hypNewsContent.Text = content;


            // 日期
            Literal ltrDate = e.Item.FindControl("ltrDate") as Literal;
            ltrDate.Text = drv["news_onlinedate"].ToString();
        }
    }

    #endregion

    /// <summary>
    /// 去搜尋_20161103_howard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_goToSearch_Click(object sender, EventArgs e)
    {
        Response.Write("<script>window.parent.location='" + "../WebPage/SemanticSearch.aspx?loginName=" + lbl_loginName.Text + "'</script>");
    }
    /// <summary>
    /// 登出_20161103_howard
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_logout_Click(object sender, EventArgs e)
    {

        Session["user_id"] = "";
        Session["user_st"] = "";
        Response.Write("<script>window.parent.location='" + "../WebPage/index.aspx" + "'</script>");
        lbtn_logout.Visible = false;

    }



}