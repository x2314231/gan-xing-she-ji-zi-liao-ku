﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for User_Model
/// </summary>
public class User_Model
{
    public User_Model()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public string user_id { set; get; }
    public string user_st { set; get; }
    public string user_name { set; get; }
    public string user_tel { set; get; }
    public string user_comName { set; get; }
    public string user_comNo { set; get; }
    public string user_address { set; get; }
    public string user_cdate { set; get; }
}