﻿
using System.Collections.Generic;

public class Rootobject
{
    public List<Result> results { get; set; }
    public string query { get; set; }
    public string imageterms { get; set; }
    public string lang { get; set; }
    public string locations { get; set; }
    public string area { get; set; }
    public int total_found { get; set; }
    public string products { get; set; }
    public int num_found { get; set; }


}

public class Result
{
    
    public string currency { get; set; }
    public string conditions { get; set; }
    public string lang { get; set; }
    public string title { get; set; }
    public float match_title { get; set; }
    public float price { get; set; }
    public float avg_score { get; set; }
    public string link { get; set; }
    public int num_reviews { get; set; }
    public string imUrl { get; set; }
    public string text { get; set; }
    public string area { get; set; }
    public string title_fix { get; set; }
    public string lang_fix { get; set; }

    public List<object> image_term_distribution { get; set; }


}
