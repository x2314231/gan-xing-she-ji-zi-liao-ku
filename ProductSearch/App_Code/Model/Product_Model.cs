﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Product_Model
/// </summary>
public class Product_Model
{
    public Product_Model()
    {
        //
        // TODO: Add constructor logic here
        //
    }


    public int p_id { set; get; }
    public string p_name { set; get; }
    public string p_imgUrl { set; get; }
    public string p_price{ set; get; }
    public string p_review { set; get; }
    public string p_oriUrl { set; get; }
    public string p_cdate{ set; get; }

}