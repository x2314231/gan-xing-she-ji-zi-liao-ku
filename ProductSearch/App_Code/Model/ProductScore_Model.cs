﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ProductScore_Model
/// </summary>
public class ProductScore_Model
{
    public ProductScore_Model()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public int ps_id { set; get; }
    public int p_id { set; get; }
    public string ps_termLeader { set; get; }
    public string ps_score { set; get; }
    public string ps_cdate { set; get; }
}