﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

/// <summary>
/// Summary description for ImagePicture_DAL
/// </summary>
public class ImagePicture_DAL:common
{
    public ImagePicture_DAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /// <summary>
    /// 取得產品基本資訊以及X Y分數_20161025_howard
    /// </summary>
    /// <param name="productScore"></param>
    /// <returns></returns>
    public DataView selectProductInfo(ProductScore_Model productScore)
    {
        try
        {
            string sqlStr = @"
select P.p_name, P.p_imgUrl, PS.ps_termLeader, PS.ps_score 
from Product P left join ProductScore PS on P.p_id=PS.p_id
where P.p_id=@p_id and (PS.ps_termLeader=@ps_termLeader_x or PS.ps_termLeader=@ps_termLeader_y)
			";

            SqlCommand sqlCmd = new SqlCommand(sqlStr);
            sqlCmd.Parameters.Add(new SqlParameter("@p_id", productScore.p_id));

            string[] ps_termLeader= Regex.Split(productScore.ps_termLeader, "_xy_", RegexOptions.IgnoreCase);
            sqlCmd.Parameters.Add(new SqlParameter("@ps_termLeader_x", ps_termLeader[0]));
            sqlCmd.Parameters.Add(new SqlParameter("@ps_termLeader_y", ps_termLeader[1]));
            return runParaCmd(sqlCmd);

        }
        catch (Exception ex)
        {
            throw new Exception(string.Format("讀取所有購物車內容，錯誤訊息:{0}", ex.Message));
        }
    }
}