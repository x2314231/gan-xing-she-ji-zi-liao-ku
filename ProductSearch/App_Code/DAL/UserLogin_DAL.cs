﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for UserLogin_DAL
/// </summary>
public class UserLogin_DAL:common
{
    public UserLogin_DAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    /// <summary>
    /// 比對帳號密碼傳回符合數量，1正確0錯誤_20161103_howard
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    public int selectUser(User_Model user)
    {
        try
        {
            string sqlStr = @"
select count(user_id) from [User] where user_id=@user_id and user_pd=@user_st
			";

            SqlCommand sqlCmd = new SqlCommand(sqlStr);
            sqlCmd.Parameters.Add(new SqlParameter("@user_id", user.user_id));
            sqlCmd.Parameters.Add(new SqlParameter("@user_st", user.user_st));
            return Convert.ToInt32(runScalar(sqlCmd));

        }
        catch (Exception ex)
        {
            throw new Exception(string.Format("比對帳號密碼傳回符合數量，錯誤訊息:{0}", ex.Message));
        }
    }
}