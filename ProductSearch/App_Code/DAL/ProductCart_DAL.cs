﻿using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for ProductCart_DAL
/// </summary>
public class ProductCart_DAL:common
{
	public ProductCart_DAL()
	{
		//
		// TODO: Add constructor logic here
		//
	}

	/// <summary>
	/// 讀取所有購物車內容_20161017_howard
	/// </summary>
	/// <param name=""></param>
	/// <returns></returns>
	public DataView getCartContent(Cart_Model cart)
	{
		try
		{
			string sqlStr = @"
select CCC.c_id,CCC.c_name,p_imgUrl,CCC.c_cdate
from Product P join (select C.c_id,C.c_name,CC.p_id,C.c_cdate from Cart C join CartContent CC 
on C.c_id=CC.c_id where C.user_id=@user_id) CCC on P.p_id=CCC.p_id
order by CCC.c_cdate desc
			";

			SqlCommand sqlCmd = new SqlCommand(sqlStr);
			sqlCmd.Parameters.Add(new SqlParameter("@user_id", cart.user_id));
			return runParaCmd(sqlCmd);

		}
		catch (Exception ex)
		{
			throw new Exception(string.Format("讀取所有購物車內容，錯誤訊息:{0}", ex.Message));
		}
	}

    /// <summary>
    /// 此用戶收藏夾名稱是否已存在
    /// </summary>
    /// <param name="cart"></param>
    /// <returns></returns>
    public Boolean isCartNameExist(string cartName, string user_id)
    {
        try
        {
            string sqlStr = @"SELECT c_name FROM Cart WHERE c_name=@c_name and user_id=@user_id";

            SqlCommand sqlCmd = new SqlCommand(sqlStr);
            sqlCmd.Parameters.Add(new SqlParameter("@c_name", cartName));
            sqlCmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            return runParaCmd(sqlCmd).Table.Rows.Count > 0;

        }
        catch (Exception ex)
        {
            throw new Exception(string.Format("檢查收藏夾名稱，錯誤訊息:{0}", ex.Message));
        }
    }



    /// <summary>
    /// 修改購物車名稱_20161018_howard
    /// </summary>
    /// <param name="cart"></param>
    public void updateCartName(Cart_Model cart)
	{
		try
		{
			string sqlStr = @"
update Cart set c_name =@c_name
where c_id=@c_id
			";
			SqlCommand sqlCmd = new SqlCommand(sqlStr);
			sqlCmd.Parameters.Add(new SqlParameter("@c_name", cart.c_name));
			sqlCmd.Parameters.Add(new SqlParameter("@c_id", cart.c_id));
		 
			runParaCmd1(sqlCmd);

		}
		catch (Exception ex)
		{
			throw new Exception(string.Format("修改購物車名稱，錯誤訊息:{0}", ex.Message));
		}
	}

	/// <summary>
	/// 刪除購物車_20161018_howard
	/// </summary>
	/// <param name="c_id"></param>
	/// <returns></returns>
	public bool deleteCart(Cart_Model cart)
	{
		try
		{
			string sqlStr = @"
SET XACT_ABORT ON
BEGIN TRANSACTION

DELETE FROM Cart where c_id=@c_id;
DELETE FROM CartContent where c_id=@c_id;

IF @@error<>0
BEGIN 
	ROLLBACK TRANSACTION
	SELECT 'N'
END
ELSE
BEGIN
	COMMIT TRANSACTION
	SELECT 'Y'
END
			";

			SqlCommand sqlCmd = new SqlCommand(sqlStr);
			sqlCmd.Parameters.Add(new SqlParameter("@c_id", cart.c_id));
			return runScalar(sqlCmd).ToString().Equals("Y");

		}
		catch (Exception ex)
		{
			throw new Exception(string.Format("刪除購物車，錯誤訊息:{0}", ex.Message));
		}
	}


	/// <summary>
	/// 點選後取得購物車所有產品圖片_20161018_howard
    /// 產品網址也撈出來_20161026_howard
	/// </summary>
	/// <param name="cart"></param>
	/// <returns></returns>
	public DataView selectCart(Cart_Model cart)
	{
		try
		{
			string sqlStr = @"
select CCC.c_id,CCC.c_name,P.p_id, p_imgUrl, P.p_name,P.p_oriUrl, P.p_review,CCC.c_cdate
from Product P join (select C.c_id,C.c_name,CC.p_id,C.c_cdate from Cart C join CartContent CC 
on C.c_id=CC.c_id where C.c_id=@c_id) CCC on P.p_id=CCC.p_id
order by CCC.c_cdate desc
			";

			SqlCommand sqlCmd = new SqlCommand(sqlStr);
			sqlCmd.Parameters.Add(new SqlParameter("@c_id", cart.c_id));
			return runParaCmd(sqlCmd);

		}
		catch (Exception ex)
		{
			throw new Exception(string.Format("讀取所有購物車內容，錯誤訊息:{0}", ex.Message));
		}
	}

	/// <summary>
	/// 取得產品的30個分數_20161020_howard
	/// </summary>
	/// <param name="product"></param>
	/// <returns></returns>
	public DataView selectProductScore(Product_Model product)
	{
		try
		{
			string sqlStr = @"
select * from ProductScore where p_id=@p_id
			";

			SqlCommand sqlCmd = new SqlCommand(sqlStr);
			sqlCmd.Parameters.Add(new SqlParameter("@p_id", product.p_id));
			return runParaCmd(sqlCmd);

		}
		catch (Exception ex)
		{
			throw new Exception(string.Format("取得產品的30個分數，錯誤訊息:{0}", ex.Message));
		}
	}
}