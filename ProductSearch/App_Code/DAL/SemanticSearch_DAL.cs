﻿using System;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for SemanticSearch_DAL
/// </summary>
public class SemanticSearch_DAL : common
{
	public SemanticSearch_DAL()
	{
		//
		// TODO: Add constructor logic here
		//
	}



	/// <summary>
	/// 取得購物車數量_20161017_howard
	/// </summary>
	/// <param name="cart"></param>
	/// <returns></returns>
	public DataView howManyCarts(Cart_Model cart)
	{
		try
		{
			string sqlStr = @"
SELECT c_name , c_id
FROM Cart
WHERE user_id=@user_id
ORDER BY c_cdate DESC
			";

			SqlCommand sqlCmd = new SqlCommand(sqlStr);
			sqlCmd.Parameters.Add(new SqlParameter("@user_id", cart.user_id));
			return runParaCmd(sqlCmd);

		}
		catch (Exception ex)
		{
			throw new Exception(string.Format("取得購物車數量，錯誤訊息:{0}", ex.Message));
		}
	}
    /// <summary>
    /// 此用戶收藏夾名稱是否已存在
    /// </summary>
    /// <param name="cart"></param>
    /// <returns></returns>
    public Boolean isCartNameExist(string cartName ,string user_id)
    {
        try
        {
            string sqlStr = @"SELECT c_name FROM Cart WHERE c_name=@c_name and user_id=@user_id";

            SqlCommand sqlCmd = new SqlCommand(sqlStr);
            sqlCmd.Parameters.Add(new SqlParameter("@c_name", cartName));
            sqlCmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            return runParaCmd(sqlCmd).Table.Rows.Count > 0;

        }
        catch (Exception ex)
        {
            throw new Exception(string.Format("檢查收藏夾名稱，錯誤訊息:{0}", ex.Message));
        }
    }



    /// <summary>
    /// 新增資料進購物車資料表 Cart_20161017_howard
    /// </summary>
    /// <param name="cart"></param>
    /// <returns></returns>
    public int InsertIntoCart(Cart_Model cart)
	{
		try
		{
			string sqlStr = @"
SET XACT_ABORT ON
BEGIN TRANSACTION

	INSERT Cart (c_name, c_conditions, c_imgTerms, user_id, c_cdate) 
	VALUES (@c_name, @c_conditions, @c_imgTerms, @user_id, GETDATE())

	DECLARE @c_id int
	SELECT @c_id =  @@IDENTITY

IF @@error<>0
BEGIN 
	ROLLBACK TRANSACTION
	SELECT 0
END
ELSE
BEGIN
	COMMIT TRANSACTION
	SELECT @c_id 
END
			";
			SqlCommand sqlCmd = new SqlCommand(sqlStr);
			sqlCmd.Parameters.Add(new SqlParameter("@c_name", cart.c_name));
			sqlCmd.Parameters.Add(new SqlParameter("@c_conditions", cart.c_conditions));
			sqlCmd.Parameters.Add(new SqlParameter("@c_imgTerms", cart.c_imgTerms));
			sqlCmd.Parameters.Add(new SqlParameter("@user_id", cart.user_id));
			return Convert.ToInt32(runScalar(sqlCmd));
		}
		catch (Exception ex)
		{
			throw new Exception(string.Format("新增資料進購物車資料表 Cart，錯誤訊息{0}", ex));
		}
	}


	/// <summary>
	/// 新增產品資料進 Product_20161017_howard
    /// 產品資料新增原始網址_20161026_howard
	/// </summary>
	/// <param name="product"></param>
	/// <returns></returns>
	public int InsertIntoProduct(Product_Model product)
	{
		try
		{
			string sqlStr = @"
SET XACT_ABORT ON
BEGIN TRANSACTION

IF EXISTS(select p_imgUrl from Product where p_imgUrl=@p_imgUrl)
BEGIN
	SELECT p_id from Product where p_imgUrl=@p_imgUrl
END
ELSE
BEGIN	
	INSERT Product (p_name, p_imgUrl, p_price, p_review, p_cdate, p_oriUrl) 
	VALUES (@p_name, @p_imgUrl, @p_price, @p_review, GETDATE(), @p_oriUrl)
	DECLARE @p_id int
	SELECT @p_id =  @@IDENTITY

IF @@error<>0
BEGIN 
	ROLLBACK TRANSACTION
	SELECT 0
END
ELSE
BEGIN
	COMMIT TRANSACTION
	SELECT @p_id
END

END


			";
			SqlCommand sqlCmd = new SqlCommand(sqlStr);
			sqlCmd.Parameters.Add(new SqlParameter("@p_name", product.p_name));
			sqlCmd.Parameters.Add(new SqlParameter("@p_imgUrl", product.p_imgUrl));
			sqlCmd.Parameters.Add(new SqlParameter("@p_price", product.p_price));
			sqlCmd.Parameters.Add(new SqlParameter("@p_review", product.p_review));
            sqlCmd.Parameters.Add(new SqlParameter("@p_oriUrl", product.p_oriUrl));
            return Convert.ToInt32(runScalar(sqlCmd));
		}
		catch (Exception ex)
		{
			throw new Exception(string.Format("新增產品資料進 Product，錯誤訊息{0}", ex));
		}
	}



	/// <summary>
	/// 新增資料進產品語意分數資料表 ProductScore_20161017_howard
	/// </summary>
	/// <param name="cart"></param>
	/// <returns></returns>
	public int InsertIntoProductScore(Product_Model product, string ps_termLeader, double ps_score)
	{
		try
		{
			string sqlStr = @"
SET XACT_ABORT ON
BEGIN TRANSACTION
IF EXISTS(select p_id from ProductScore where p_id=@p_id and ps_termLeader=@ps_termLeader)
BEGIN
	SELECT ps_id from ProductScore where p_id=@p_id and ps_termLeader=@ps_termLeader
END
ELSE
BEGIN
	INSERT ProductScore (p_id, ps_termLeader, ps_score, ps_cdate) 
	VALUES (@p_id, @ps_termLeader, @ps_score, GETDATE())

	DECLARE @ps_id int
	SELECT @ps_id =  @@IDENTITY
END
IF @@error<>0
BEGIN 
	ROLLBACK TRANSACTION
	SELECT 0
END
ELSE
BEGIN
	COMMIT TRANSACTION
	SELECT @ps_id 
END
			";
			SqlCommand sqlCmd = new SqlCommand(sqlStr);
			sqlCmd.Parameters.Add(new SqlParameter("@p_id", product.p_id));
			sqlCmd.Parameters.Add(new SqlParameter("@ps_termLeader", ps_termLeader));
			sqlCmd.Parameters.Add(new SqlParameter("@ps_score", ps_score));
			return Convert.ToInt32(runScalar(sqlCmd));
		}
		catch (Exception ex)
		{
			throw new Exception(string.Format("新增資料進產品語意分數資料表 ProductScore，錯誤訊息{0}", ex));
		}
	}

	/// <summary>
	/// 新增資料進購物車內容資料表 CartContent_20161017_howard
	/// </summary>
	/// <param name="cartContent"></param>
	/// <returns></returns>
	public int InsertIntoCartContent(CartContent_Model cartContent)
	{
		try
		{
			string sqlStr = @"
SET XACT_ABORT ON
BEGIN TRANSACTION
IF  EXISTS(select p_id from CartContent where p_id=@p_id and c_id=@c_id)
BEGIN 
	ROLLBACK TRANSACTION
	SELECT 0
END
ELSE
BEGIN
	INSERT CartContent (c_id, p_id) 
	VALUES (@c_id, @p_id)
	DECLARE @cc_id int
	SELECT @cc_id =  @@IDENTITY
END
IF @@error<>0
BEGIN 
	ROLLBACK TRANSACTION
	SELECT 0
END
ELSE
BEGIN
	COMMIT TRANSACTION
	SELECT @cc_id 
END
			";
			SqlCommand sqlCmd = new SqlCommand(sqlStr);
			sqlCmd.Parameters.Add(new SqlParameter("@c_id", cartContent.c_id));
			sqlCmd.Parameters.Add(new SqlParameter("@p_id", cartContent.p_id));
			return Convert.ToInt32(runScalar(sqlCmd));
		}
		catch (Exception ex)
		{
			throw new Exception(string.Format("新增資料進購物車內容資料表 CartContent，錯誤訊息{0}", ex));
		}
	}
}