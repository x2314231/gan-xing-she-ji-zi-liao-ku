﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AddUser_DAL
/// </summary>
public class AddUser_DAL:common
{
    public AddUser_DAL()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    /// <summary>
    /// 新增會員資料進 User_20161103_howard
    /// </summary>
    /// <param name="user"></param>
    /// <returns></returns>
    public int InsertIntoUser(User_Model user)
    {
        try
        {
            string sqlStr = @"
SET XACT_ABORT ON
BEGIN TRANSACTION

IF EXISTS(select user_id from [User] where user_id=@user_id)
BEGIN
	SELECT 0
END
ELSE
BEGIN	
	INSERT into [User] (user_id, user_pd, user_name, user_tel, user_comName, user_comNo, user_address, user_cdate) 
	VALUES (@user_id, @user_st, @user_name, @user_tel, @user_comName, @user_comNo, @user_address, GETDATE())

IF @@error<>0
BEGIN 
	ROLLBACK TRANSACTION
	SELECT 0
END
ELSE
BEGIN
	COMMIT TRANSACTION
	SELECT 1
END

END

			";
            SqlCommand sqlCmd = new SqlCommand(sqlStr);
            sqlCmd.Parameters.Add(new SqlParameter("@user_id", user.user_id));
            sqlCmd.Parameters.Add(new SqlParameter("@user_st", user.user_st));
            sqlCmd.Parameters.Add(new SqlParameter("@user_name", user.user_name));
            sqlCmd.Parameters.Add(new SqlParameter("@user_tel", user.user_tel));
            sqlCmd.Parameters.Add(new SqlParameter("@user_comName", user.user_comName));
            sqlCmd.Parameters.Add(new SqlParameter("@user_comNo", user.user_comNo));
            sqlCmd.Parameters.Add(new SqlParameter("@user_address", user.user_address));

            return Convert.ToInt32(runScalar(sqlCmd));
        }
        catch (Exception ex)
        {
            throw new Exception(string.Format("新增會員資料進 User，錯誤訊息{0}", ex));
        }
    }
}