﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

public partial class SubWin_SearchBar : System.Web.UI.Page
{
    public string Query = "";
    public string AreaMenu = "";
    public string LocationMenu = "";
    public string price_lower = "";
    public string price_upper = "";
    public string lang = "";
    public int page = 1;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        lbtn_close.Attributes.Add("onclick", "javascript: parent.$.colorbox.close();return true;");
        
    }
    /// <summary>
    /// call api
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lbtn_search_OnClick(object sender, EventArgs e)
    {
        if (tbx_query.Text == "")
        {
            Response.Write("<script>alert('搜尋條件必填！')</script>");
        }
        else
        {
            //參數
            Query = tbx_query.Text;

            if (rbtn_area_living.Checked)
            {
                AreaMenu = "living";
            }
            else if (rbtn_area_mombaby.Checked)
            {
                AreaMenu = "mombaby";
            }
            else if (rbtn_area_office.Checked)
            {
                AreaMenu = "office";
            }
            else if (rbtn_area_leisure.Checked)
            {
                AreaMenu = "leisure";
            }


            if (rbtn_location_kitchen.Checked)
            {
                LocationMenu = "廚房";
            }
            else if (rbtn_location_livingroom.Checked)
            {
                LocationMenu = "客廳";
            }
            else if (rbtn_location_garden.Checked)
            {
                LocationMenu = "花園";
            }

            price_lower = tbx_price_lower.Text;
            price_upper = tbx_price_upper.Text;

            if (cbx_lang_en.Checked)
            {
                lang = "en";
            }
            else if (cbx_lang_zh.Checked)
            {
                lang = "zh";
            }

            //hf_jsonQuery.Value = "../WebPage/SemanticSearch.aspx?&Query=" + Query + "&Area=" + AreaMenu + "&Location=" + LocationMenu + "&price_lower=" + price_lower + "&price_upper=" + price_upper + "&lang=" + lang;
            Response.Write("<script>window.parent.location='" + "../WebPage/SemanticSearch.aspx?&Query=" + Query + "&Area=" + AreaMenu + "&Location=" + LocationMenu + "&price_lower=" + price_lower + "&price_upper=" + price_upper + "&page=" + page + "&lang=" + lang + "'</script>");
            Page.ClientScript.RegisterStartupScript(this.GetType(), "showLoading", "showLoading()", true);

        }
    }
    protected void lbtn_close_OnClick(object sender, EventArgs e)

    {
        //div_loading.Attributes["style"] = "display:block";
        Page.ClientScript.RegisterStartupScript(this.GetType(), "showLoading", "showLoading()", true);
         
        //Response.Write("<script>document.getElementById('div_loading').style.display = 'block';</script>");
        //Response.Write("<script>window.parent.location='../WebPage/SemanticSearch.aspx'</script>");
    }
}