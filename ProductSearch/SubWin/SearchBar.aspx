﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SearchBar.aspx.cs" Inherits="SubWin_SearchBar" EnableEventValidation="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>感性設計使用者聯盟</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.css" rel="stylesheet"> 
    <!-- jQuery load -->
    <script src="../js/jquery.min.js"></script>

    <!-- Custom styles for this template -->
    <link href="../style.css" rel="stylesheet">
    <link href="../css/plugin.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">
    <link href="../css/animate.min.css" rel="stylesheet">
    <link href="../css/responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/colorbox.css" media="all" />
    <!-- Revolutions slider -->

    <link href="../rs-plugin/css/settings.css" rel="stylesheet">
    <link href="../rs-plugin/css/captions.css" rel="stylesheet">
    <!-- multi selector -->
    <link href="../css/multiple-select.css" rel="stylesheet">
    <link href="../css/jquery.powertip.css" rel="stylesheet" type="text/css" />


    <!-- 160225 add start -->
    <link href="../css/dcaccordion.css" rel="stylesheet">
    <link href="../css/skins/grey.css" rel="stylesheet" type="text/css" />

    <script type='text/javascript' src='../js/jquery.checkall.js'></script>
    <script type='text/javascript' src='../js/jquery.cookie.js'></script>
    <script type="text/javascript" src="../js/jquery.hoverIntent.minified.js"></script>
    <script type="text/javascript" src="../js/jquery.dcjqaccordion.2.7.min.js"></script>
    <script type="text/javascript" src="../js/masonry.pkgd.min.js"></script>
    <script type='text/javascript' src="../js/bootstrap.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            //checkall
            $('.check-all').checkAll();

            $('.accordion-1').dcAccordion({
                eventType: 'click',
                autoClose: false,
                saveState: false,
                disableLink: true,
                showCount: true,
                speed: 'slow'
            });


            $(".class").colorbox({
                iframe: true,
                onClosed: function () { location.reload(true); }
            });

            /* 頁籤式選單:若單頁出現兩個以上,需再加入ID */
            $(".tabmenuT2Btn").click(function () {
                $(".tabmenuT2Btn").removeClass("tabmenuT2BtnCurrent");
                $(this).addClass("tabmenuT2BtnCurrent");

            })

        });

        function showLoading() {
             
            document.getElementById('div_loading').style.display = 'block'; 

        }
    </script>
    <!-- 160225 add end -->
     <style>
.rcorners1 {
    border-radius: 25px;
    border: 2px solid #73AD21;
    padding: 10px; 
    margin:10px
}
.loader {
    border: 4px solid #f3f3f3; /* Light grey */
    border-top: 4px solid #3498db; /* Blue */
    border-radius: 50%;
    width: 30px;
    height: 30px;
    animation: spin 2s linear infinite;
    position: relative;
    top:47%;
    left:47%; 

}

@keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
}
.loading_span{

    position: absolute;
    left:42%;
    top:60%;
    color:white;
    font-size:20px
}

</style>
</head>
<body runat="server" >
     
    <form id="SearchBar" class="con" runat="server" >
      
        <div id="div_loading" style="width:100%;height:100%; background-color:rgba(0, 0, 0, 0.44);position:absolute;z-index:1;display:none">
             <div  class="loader"></div>
             <span class="loading_span">Searching...</span>
        </div>
        <div id="searchblock" runat="server" class="container"  >
            
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <span class="font-size3">搜尋條件:</span><asp:TextBox ID="tbx_query" class="inputline" Text="" runat="server" meta:resourcekey="tbx_queryResource1"></asp:TextBox>
                    <asp:LinkButton ID="LinkButton1" class="genbtnS" runat="server" Text="取消" OnClick="lbtn_close_OnClick" meta:resourcekey="lbtn_closeResource1"></asp:LinkButton>
                    <asp:LinkButton ID="LinkButton2" class="genbtnS" runat="server" Text="搜尋" OnClick="lbtn_search_OnClick" meta:resourcekey="lbtn_searchResource1"></asp:LinkButton>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 rcorners1" >

                    <h1>產品別</h1>
                    <br />
                    <asp:RadioButton ID="RadioButton1" GroupName="AreaMenu" Text="居家生活" runat="server" />

                    <asp:RadioButton ID="RadioButton2" GroupName="AreaMenu" Text="母嬰" runat="server" />

                    <asp:RadioButton ID="RadioButton3" GroupName="AreaMenu" Text="辦公" runat="server" />

                    <asp:RadioButton ID="RadioButton4" GroupName="AreaMenu" Text="音樂運動休閒" runat="server" />


                </div> 
                <div class="col-lg-3  col-md-3 col-sm-3 col-xs-3 rcorners1">
                    
                    <h1>地點</h1>
                    <br />
                    <asp:RadioButton ID="RadioButton5" GroupName="LocationMenu" Text="廚房" runat="server" />

                    <asp:RadioButton ID="RadioButton6" GroupName="LocationMenu" Text="客廳" runat="server" />

                    <asp:RadioButton ID="RadioButton7" GroupName="LocationMenu" Text="花園" runat="server" />

                </div>
             </div>

                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 rcorners1" >

                        <h1>價格  </h1>
                        <br />
                        <asp:RadioButton ID="RadioButton8" GroupName="AreaMenu" Text="居家生活" runat="server" />

                        <asp:RadioButton ID="RadioButton9" GroupName="AreaMenu" Text="母嬰" runat="server" />

                        <asp:RadioButton ID="RadioButton10" GroupName="AreaMenu" Text="辦公" runat="server" />

                        <asp:RadioButton ID="RadioButton11" GroupName="AreaMenu" Text="音樂運動休閒" runat="server" />


                    </div> 
                    <div class="col-lg-3  col-md-3 col-sm-3 col-xs-3 rcorners1">
                    
                        <h1>資料來源</h1>
                        <br />
                        <asp:RadioButton ID="RadioButton12" GroupName="LocationMenu" Text="廚房" runat="server" />

                        <asp:RadioButton ID="RadioButton13" GroupName="LocationMenu" Text="客廳" runat="server" />

                        <asp:RadioButton ID="RadioButton14" GroupName="LocationMenu" Text="花園" runat="server" />

                    </div>
         </div>
        
        <!-- easytab start -->
            <!--
        <div id="tab-container" class='easytabH margin10T'>
            <ul class="menubar">
                <li><a href="#tabs1">產品</a></li>
                <li><a href="#tabs2">使用地點</a></li>
                <li><a href="#tabs3">價格</a></li>
                <li><a href="#tabs4">資料來源</a></li>
            </ul>
            <div class='panel-container'>
                 
                <div id="tabs1">


                    <asp:RadioButton ID="rbtn_area_living" GroupName="AreaMenu" Text="居家生活" runat="server" />

                    <asp:RadioButton ID="rbtn_area_mombaby" GroupName="AreaMenu" Text="母嬰" runat="server" />

                    <asp:RadioButton ID="rbtn_area_office" GroupName="AreaMenu" Text="辦公" runat="server" />

                    <asp:RadioButton ID="rbtn_area_leisure" GroupName="AreaMenu" Text="音樂運動休閒" runat="server" />


                </div>
                 
                <div id="tabs2">

                    <asp:RadioButton ID="rbtn_location_kitchen" GroupName="LocationMenu" Text="廚房" runat="server" />

                    <asp:RadioButton ID="rbtn_location_livingroom" GroupName="LocationMenu" Text="客廳" runat="server" />

                    <asp:RadioButton ID="rbtn_location_garden" GroupName="LocationMenu" Text="花園" runat="server" />

                </div>
                 
                <div id="tabs3">

                    <asp:TextBox ID="tbx_price_lower" Text="-1" runat="server" meta:resourcekey="tbx_price_lower_oldResource1"></asp:TextBox>&nbsp;0~999 NT
                        
                            <asp:TextBox ID="tbx_price_upper" Text="100000" runat="server" meta:resourcekey="tbx_price_upper_oldResource1"></asp:TextBox>&nbsp;1,000~9,999 NT
                       
                </div> 
                <div id="tabs4">

                    <asp:CheckBox ID="cbx_lang_en" runat="server" Text="美國" />

                    <asp:CheckBox ID="cbx_lang_zh" runat="server" Text="中國" />

                </div> 
            </div> 
        </div> 

            
        <div class="twocol margin10T">
            <div class="right">
                <%--<input type="submit" class="genbtnS" value="關閉" onclick="javascript: parent.$.colorbox.close();" />--%>
                <asp:LinkButton ID="lbtn_close" class="genbtnS" runat="server" Text="取消" OnClick="lbtn_close_OnClick" meta:resourcekey="lbtn_closeResource1"></asp:LinkButton>
                <%--<button class="closecolorbox">取消</button>&nbsp;--%>
                <%--<asp:Button ID="btn_search" runat="server" Text="搜尋" OnClick="lbtn_search_OnClick" meta:resourcekey="btn_searchResource1"></asp:Button>--%>
                <asp:LinkButton ID="lbtn_search" class="genbtnS" runat="server" Text="搜尋" OnClick="lbtn_search_OnClick" meta:resourcekey="lbtn_searchResource1"></asp:LinkButton>
            </div>
        </div> 
                -->
        <asp:HiddenField ID="hf_jsonQuery" runat="server" />

        </div>
        <script type="text/javascript" src="../js/jquery-migrate-1.2.1.js"></script>
        <script type="text/javascript" src="../js/jquery.colorbox.js"></script>
        <script type="text/javascript" src="../js/function.js"></script>
        <script type="text/javascript" src="../js/jquery.easytabs.min.js"></script>
        <!-- easytabs tab -->
        <!-- powertip:tooltips -->
        <script type="text/javascript">

            $('#tab-container').easytabs({

            });
            //點選切換樣式
            $(".shoppingcheck").click(function () {
                $(this).toggleClass("shoppingcheckup");
            })
        </script>
    </form>
</body>
</html>
