function initMatrix(json, xName, yName) {
    var margin = { top: 50, right: 50, bottom: 50, left: 50 },
    outerWidth = 1400,
    outerHeight = 680,
    width = outerWidth - margin.left - margin.right,
    height = outerHeight - margin.top - margin.bottom;

    var x = d3.scale.linear()
        .rangeRound([0, width]);

    var y = d3.scale.linear()
        .rangeRound([height, 0]);

    var xCat = xName,
        yCat = yName,
        image = "p_imgUrl",
        name = "p_name";

    d3.json(json, function (data) {
        data.forEach(function (d) {
            d.p_name = d.p_name;
            d.p_imgUrl = d.p_imgUrl;
            d.ps_score_x = +d.ps_score_x;
            d.ps_score_y = +d.ps_score_y;
            d.width = 150;
            d.height = 150;
        });

        var xMax = d3.max(data, function (d) { return d[xCat]; }) * 1.05,
            xMin = d3.min(data, function (d) { return d[xCat]; }),
            xMin = xMin > 0 ? 0 : xMin,
            yMax = d3.max(data, function (d) { return d[yCat]; }) * 1.05,
            yMin = d3.min(data, function (d) { return d[yCat]; }),
            yMin = yMin > 0 ? 0 : yMin;

        x.domain([xMin, xMax]);
        y.domain([yMin, yMax]);

        var xAxis = d3.svg.axis()
            .scale(x)
            .orient("bottom")
            .tickSize(-height)
            .tickValues([100]);

        var yAxis = d3.svg.axis()
            .scale(y)
            .orient("left")
            .tickSize(-width)
            .tickValues([100]);

        var color = d3.scale.category10();

        var tip = d3.tip()
            .attr("class", "d3-tip")
            .offset([-10, 0])
            .html(function (d) {
                //return name + ": " + d[name] + "<br>" + xCat + ": " + d[xCat] + "<br>" + yCat + ": " + d[yCat];
                //return "Name: " + d[name] + "<br>" + xCat + ": " + d[xCat] + "<br>" + yCat + ": " + d[yCat];
                return "Name: " + d[name];
            });

        var zoomBeh = d3.behavior.zoom()
            .x(x)
            .y(y)
            .scaleExtent([0, 500])
            .on("zoom", zoom);

        var svg = d3.select("#scatter")
            .append("svg")
            .attr("width", outerWidth)
            .attr("height", outerHeight)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
            .call(zoomBeh);

        svg.call(tip);

        svg.append("rect")
            .attr("width", width)
            .attr("height", height);

        svg.append("g")
            .classed("x axis", true)
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
            .append("text")
            .classed("label", true)
            .attr("x", width)
            .attr("y", margin.bottom - 10)
            .style("text-anchor", "end")
            .text(xCat);

        svg.append("g")
            .classed("y axis", true)
            .call(yAxis)
            .append("text")
            .classed("label", true)
            .attr("transform", "rotate(-90)")
            .attr("y", -margin.left)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text(yCat);

        var objects = svg.append("svg")
            .classed("objects", true)
            .attr("width", width)
            .attr("height", height);

        objects.append("svg:line")
            .classed("axisLine hAxisLine", true)
            .attr("x1", 0)
            .attr("y1", 0)
            .attr("x2", width)
            .attr("y2", 0)
            .attr("transform", "translate(0," + height + ")");

        objects.append("svg:line")
            .classed("axisLine vAxisLine", true)
            .attr("x1", 0)
            .attr("y1", 0)
            .attr("x2", 0)
            .attr("y2", height);

        var node = objects.selectAll("image")
            .data(data)
            .enter()
            .append("svg:image")
            //.attr("xlink:href", function (d) { return 'img/ice_cream/' + d[image] + '.png'; })
            .attr("xlink:href", function (d) { return d[image]; })
            //.attr("transform", transform)
            .attr("width", function (d) { return d.width; })
            .attr("height", function (d) { return d.height; })
            //.style("fill", function(d) { return color(d[name]); })
            .classed("node", true)
            .on("mouseover", tip.show)
            .on("mouseout", tip.hide);

        var legend = svg.selectAll(".legend")
            .data(color.domain())
            .enter().append("g")
            .classed("legend", true)
            .attr("transform", function (d, i) { return "translate(0," + i * 20 + ")"; });

        legend.append("circle")
            .attr("r", 3.5)
            .attr("cx", width + 20)
            .attr("fill", color);

        legend.append("text")
            .attr("x", width + 26)
            .attr("dy", ".35em")
            .text(function (d) { return d; });


        function zoom() {
            svg.select(".x.axis").call(xAxis);
            svg.select(".y.axis").call(yAxis);
            node.each(function (d) {
                d.x = +x(d[xCat]);
                d.y = +y(d[yCat]);
            });
            node.sort(function (a, b) { return a.x - b.x || a.y - b.y; });
            node.each(collide());

            node.attr("x", function (d) { return d.x; })
                .attr("y", function (d) { return d.y; });
        }

        function transform(d) {
            return "translate(" + (x(d[xCat])) + "," + (y(d[yCat])) + ")";
        }

        // Set initial positions
        node.each(function (d) {
            d.x = +x(d[xCat]);
            d.y = +y(d[yCat]);
        });
        node.sort(function (a, b) { return a.x - b.x || a.y - b.y; });
        node.each(collide());
        node.attr("x", function (d) { return d.x; })
            .attr("y", function (d) { return d.y; });


        function collide() {
            return function (d) {
                var xOffset = 0;
                var yOffset = 0;
                node.each(function (e) {
                    if (d && (d !== e)) {
                        var rectA = d;
                        var rectB = e;
                        var overlap = rectOverlap(rectA, rectB);
                        console.log(d.p_name + " x:" + Math.round(d.x) + " y:" + Math.round(d.y) + "     " + e.NAME + " x:" + Math.round(e.x) + " y:" + Math.round(e.y));

                        if (overlap != '') {
                            if (xOffset != 0) {
                                if (rectA.x >= rectB.x) {
                                    rectA.x += xOffset;
                                }
                                else {
                                    rectB.x += xOffset;
                                }
                            }
                            else if (yOffset != 0) {
                                if (rectA.y >= rectB.y) {
                                    rectB.y -= yOffset;
                                }
                                else {
                                    rectA.y -= yOffset;
                                }
                            }
                            //else if
                            if (overlap == 'x') {
                                if (rectA.x >= rectB.x) {
                                    xOffset = rectB.x + rectB.width - rectA.x;
                                    rectA.x += xOffset;
                                }
                                else {
                                    xOffset = rectA.x + rectA.width - rectB.x;
                                    rectB.x += xOffset;
                                }
                            }
                            else if (overlap == 'y') {
                                if (rectA.y >= rectB.y) {
                                    yOffset = rectB.y + rectB.height - rectA.y;
                                    rectB.y -= yOffset;
                                }
                                else {
                                    yOffset = rectA.y + rectA.height - rectB.y;
                                    rectA.y -= yOffset;
                                }
                            }
                        }
                        console.log(d.p_name + " x:" + Math.round(d.x) + " y:" + Math.round(d.y) + "     " + e.NAME + " x:" + Math.round(e.x) + " y:" + Math.round(e.y));

                        function valueInRange(value, min, max) {
                            if ((value < max) && (value >= min))
                                return value - min;
                            return -1;
                        }

                        function rectOverlap(A, B) {
                            var xOverlap = -1, yOverlap = -1;
                            var len1, len2;

                            len1 = valueInRange(A.x, B.x, B.x + B.width);
                            len2 = valueInRange(B.x, A.x, A.x + A.width);
                            if (len1 > -1 || len2 > -1) {
                                xOverlap = Math.max(len1, len2);
                            }

                            len1 = valueInRange(A.y, B.y, B.y + B.height);
                            len2 = valueInRange(B.y, A.y, A.y + A.height);
                            if (len1 > -1 || len2 > -1) {
                                yOverlap = Math.max(len1, len2);
                            }

                            if (xOverlap > -1 && yOverlap > -1) {
                                return xOverlap > yOverlap ? 'x' : 'y';
                            }
                            return '';
                        }
                    }
                })
            };
        }
    });
}

