$(document).ready(function () {
    $('#Mainmenu').menufication({
        theme: 'dark',
        showHeader: false,
        hideDefaultMenu: true,
        toggleElement: '#toggle',
        triggerWidth: 1200,
        addHomeLink: false
    });

    $('#messages').bxSlider({
        pager: false,
        controls: false,
        mode: 'vertical',
        auto: true
    });
    $('.mainslider').bxSlider({
        pagerCustom: '#main-pager',
        auto: true
    });
    $('#Newsslider1').owlCarousel({
        items: 1,
        loop: true,
        animateOut: 'fadeOutUp',
        animateIn: 'fadeIn',
        nav: true,
        navText: ['&nbsp;', '&nbsp;'],
        dots: false
    });
    $('#Newsslider2').owlCarousel({
        items: 1,
        loop: true,
        animateOut: 'fadeOutUp',
        animateIn: 'fadeIn',
        nav: true,
        navText: ['&nbsp;', '&nbsp;'],
        dots: false
    });
    $('#Newsslider3').owlCarousel({
        items: 1,
        loop: true,
        animateOut: 'fadeOutUp',
        animateIn: 'fadeIn',
        nav: true,
        navText: ['&nbsp;', '&nbsp;'],
        dots: false
    });
    $('#Nicebookslider1').owlCarousel({
        items: 1,
        loop: true,
        animateOut: 'fadeOutUp',
        animateIn: 'fadeIn',
        nav: true,
        navText: ['&nbsp;', '&nbsp;'],
        dots: false
    });
    $('#Nicebookslider2').owlCarousel({
        items: 1,
        loop: true,
        animateOut: 'fadeOutUp',
        animateIn: 'fadeIn',
        nav: true,
        navText: ['&nbsp;', '&nbsp;'],
        dots: false
    });
    $('#Topslider1').owlCarousel({
        items: 1,
        loop: true,
        animateOut: 'fadeOutUp',
        animateIn: 'fadeIn',
        nav: true,
        navText: ['&nbsp;', '&nbsp;'],
        dots: false
    });
    $('#Topslider2').owlCarousel({
        items: 1,
        loop: true,
        animateOut: 'fadeOutUp',
        animateIn: 'fadeIn',
        nav: true,
        navText: ['&nbsp;', '&nbsp;'],
        dots: false
    });
    $('#News_tab').responsiveTabs({
        startCollapsed: 'accordion'
    });
    $('#Nicebook_tab').responsiveTabs({
        startCollapsed: 'accordion'
    });
    $('#Topbook_tab').responsiveTabs({
        startCollapsed: 'accordion'
    });
    $('#Featurebook_tab').responsiveTabs({
        startCollapsed: 'accordion'
    });

    $("#fullwidthSlider").owlCarousel({
        items: 5,
        loop: true,
        autoplay: true,
        autoplayHoverPause: true,
        autoplayTimeout: 2500,
        stagePadding: 40,
        margin: 10,
        nav: true,
        navText: ['&nbsp;', '&nbsp;'],
        dots: false,
        responsive: {
            0: {
                items: 1,
            },
            768: {
                items: 3,
            },
            1200: {
                items: 5,
            }
        }
    });
});